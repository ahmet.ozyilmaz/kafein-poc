// src/dtos/index.ts

export interface CartDto {
  id: number;

  customer: CustomerDto;
  cartItems: CartItemDto[];
}

export interface CartItemDto {
  id: number;
  product: ProductDto;
  quantity: number;
}

export interface CustomerDto {
  id?: number;
  orders: OrderDto[];
  cart: CartDto;
}

export interface OrderDto {
  id: number;

  customer: CustomerDto;
  orderDate: string;
  status: string;
}

export interface OrderItemDto {
  id: number;

  order: OrderDto;
  product: ProductDto;
  quantity: number;
  unitPrice: number;
}

export interface ProductDto {
  id: number;

  name: string;
  description: string;
  price: number;
  stock: number;
  orderItems: OrderItemDto[];
  cartItems: CartItemDto[];
}

export interface MediaDto {
  id: number;

  productId: number;
  objectId: string;
  thumbnail: string;
  description: string;
  useReport: boolean;
  fileName: string;
  size: number;
}

export interface PageImplProductDto {
  content: ProductDto[];
  pageable: PageableObject;
  last: boolean;
  totalPages: number;
  totalElements: number;
  size: number;
  number: number;
  sort: Sort;
  first: boolean;
  numberOfElements: number;
  empty: boolean;
}

export interface PageableObject {
  offset: number;
  sort: Sort;
  pageNumber: number;
  pageSize: number;
  paged: boolean;
  unpaged: boolean;
}

export interface Sort {
  empty: boolean;
  sorted: boolean;
  unsorted: boolean;
}

export interface PageImplOrderDto {
  content: OrderDto[];
  pageable: PageableObject;
  last: boolean;
  totalPages: number;
  totalElements: number;
  size: number;
  number: number;
  sort: Sort;
  first: boolean;
  numberOfElements: number;
  empty: boolean;
}

export interface PageImplOrderItemDto {
  content: OrderItemDto[];
  pageable: PageableObject;
  last: boolean;
  totalPages: number;
  totalElements: number;
  size: number;
  number: number;
  sort: Sort;
  first: boolean;
  numberOfElements: number;
  empty: boolean;
}

export interface PageImplCustomerDto {
  content: CustomerDto[];
  pageable: PageableObject;
  last: boolean;
  totalPages: number;
  totalElements: number;
  size: number;
  number: number;
  sort: Sort;
  first: boolean;
  numberOfElements: number;
  empty: boolean;
}

export interface PageImplCartItemDto {
  content: CartItemDto[];
  pageable: PageableObject;
  last: boolean;
  totalPages: number;
  totalElements: number;
  size: number;
  number: number;
  sort: Sort;
  first: boolean;
  numberOfElements: number;
  empty: boolean;
}

export interface PageImplCartDto {
  content: CartDto[];
  pageable: PageableObject;
  last: boolean;
  totalPages: number;
  totalElements: number;
  size: number;
  number: number;
  sort: Sort;
  first: boolean;
  numberOfElements: number;
  empty: boolean;
}
