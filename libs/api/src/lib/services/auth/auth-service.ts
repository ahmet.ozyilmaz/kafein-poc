// libs/api-services/src/lib/api.ts
import axios from 'axios';

const instance = axios.create({
  baseURL: 'http://213.159.2.75:9001',
  headers: {
    'Content-Type': 'application/json',
  },
});
// Use an interceptor to automatically set the token for every request
instance.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem('token');
    if (token) {
      config.headers['Authorization'] = `Bearer ${token}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);
export interface UserDto {
  id: number;
  username: string;
  password: string;
  birthDate: string;
  email: string;
  phone: string;
  name: string;
  customerId: number;
}

// JwtRequest type
export interface JwtRequest {
  username: string;
  password: string;
}

// JwtResponse type
export interface JwtResponse {
  id: number;
  token: string;
}

// ResetPasswordDto type
export interface ResetPasswordDto {
  oldPassword: string;
  newPassword: string;
}

// Pagination type
export interface Pagination {
  pageNo?: number;
  pageSize?: number;
}

// PageImplUserDto type
export interface PageImplUserDto {
  content: UserDto[];
  totalPages: number;
  totalElements: number;
  last: boolean;
  size: number;
  number: number;
  numberOfElements: number;
  first: boolean;
  empty: boolean;
}

export const userService = {
  get: async (id: number): Promise<UserDto> => {
    const response = await instance.get<UserDto>(`/user/${id}`);
    return response.data;
  },
  update: async (id: number, user: UserDto): Promise<UserDto> => {
    const response = await instance.put<UserDto>(`/user/${id}`, user);
    return response.data;
  },
  delete: async (id: number): Promise<string> => {
    const response = await instance.delete<string>(`/user/${id}`);
    return response.data;
  },
  save: async (user: UserDto): Promise<void> => {
    const response = await instance.post<UserDto>('auth/save-user', user);
  },
  resetPassword: async (resetPasswordDto: ResetPasswordDto): Promise<void> => {
    await instance.post('/user/reset-password', resetPasswordDto);
  },
  createAuthenticationToken: async (
    jwtRequest: JwtRequest
  ): Promise<JwtResponse> => {
    const response = await instance.post<JwtResponse>(
      '/auth/get-token',
      jwtRequest
    );
    return response.data;
  },
  forgetPassword: async (email: string): Promise<void> => {
    await instance.post('/auth/forget-password', { params: { email } });
  },
  confirmUser: async (token: string): Promise<string> => {
    try {
      const response = await instance.post<string>('/auth/confirm-user', null, {
        params: {
          token,
        },
      });

      return response.data;
    } catch (error) {
      // Handle error cases (e.g., throw an error, return a custom error object, etc.)
      console.error('Error confirming user:', error);
      throw error;
    }
  },
  getByUsername: async (username: string): Promise<UserDto> => {
    const response = await instance.get<UserDto>(`/user/username/${username}`);
    return response.data;
  },
  getAll: async (pagination?: Pagination): Promise<PageImplUserDto> => {
    const response = await instance.get<PageImplUserDto>('/user/all', {
      params: pagination,
    });
    return response.data;
  },
};
