import { PageImplProductDto, ProductDto } from '../../models/dtos';
import baseService from '../base-service';

export const getProduct = async (id: number): Promise<ProductDto> => {
  const response = await baseService.get<ProductDto>(`/product/${id}`);
  return response.data;
};

export const updateProduct = async (
  id: number,
  productDto: ProductDto
): Promise<ProductDto> => {
  const response = await baseService.put<ProductDto>(
    `/product/${id}`,
    productDto
  );
  return response.data;
};

export const deleteProduct = async (id: number): Promise<string> => {
  const response = await baseService.delete<string>(`/product/${id}`);
  return response.data;
};
export const saveProduct = async (product: ProductDto): Promise<ProductDto> => {
  const response = await baseService.post<ProductDto>(`/product`, product);
  return response.data;
};
export const getAllProducts = async (
  pageNo = 0,
  pageSize = 10
): Promise<PageImplProductDto> => {
  const response = await baseService.get<PageImplProductDto>('/product/all', {
    params: { pageNo, pageSize },
  });

  return response.data;
};
