// libs/api/src/lib/order-service.ts
import {
  OrderDto,
  OrderItemDto,
  PageImplOrderDto,
  PageImplOrderItemDto,
} from '../../models/dtos';
import baseService from '../base-service';

export const getOrder = async (id: number): Promise<OrderDto> => {
  const response = await baseService.get<OrderDto>(`/order/${id}`);
  return response.data;
};

export const updateOrder = async (
  id: number,
  order: OrderDto
): Promise<OrderDto> => {
  const response = await baseService.put<OrderDto>(`/order/${id}`, order);
  return response.data;
};

export const deleteOrder = async (id: number): Promise<string> => {
  const response = await baseService.delete<string>(`/order/${id}`);
  return response.data;
};
export const getOrderItem = async (id: number): Promise<OrderItemDto> => {
  const response = await baseService.get<OrderItemDto>(`/order-item/${id}`);
  return response.data;
};

export const updateOrderItem = async (
  id: number,
  orderItem: OrderItemDto
): Promise<OrderItemDto> => {
  const response = await baseService.put<OrderItemDto>(
    `/order-item/${id}`,
    orderItem
  );
  return response.data;
};

export const deleteOrderItem = async (id: number): Promise<string> => {
  const response = await baseService.delete<string>(`/order-item/${id}`);
  return response.data;
};

export const getAllOrders = async (
  pageNo = 0,
  pageSize = 10
): Promise<PageImplOrderDto> => {
  const response = await baseService.get<PageImplOrderDto>('/order/all', {
    params: { pageNo, pageSize },
  });

  return response.data;
};
export const getAllOrderItems = async (
  pageNo = 0,
  pageSize = 10
): Promise<PageImplOrderItemDto> => {
  const response = await baseService.get<PageImplOrderItemDto>(
    '/order-item/all',
    {
      params: { pageNo, pageSize },
    }
  );

  return response.data;
};
