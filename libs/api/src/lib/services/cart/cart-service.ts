// libs/api/src/lib/cart-api.ts
import { CartItemDto, CartDto } from '../../models/dtos';
import baseService from '../base-service';

export const getCartItem = async (id: number): Promise<CartItemDto> => {
  const response = await baseService.get<CartItemDto>(`/cartItem/${id}`);
  return response.data;
};

export const updateCartItem = async (
  id: number,
  cartItem: CartItemDto
): Promise<CartItemDto> => {
  const response = await baseService.put<CartItemDto>(
    `/cartItem/${id}`,
    cartItem
  );
  return response.data;
};

export const deleteCartItem = async (id: number): Promise<string> => {
  const response = await baseService.delete<string>(`/cartItem/${id}`);
  return response.data;
};

export const getCart = async (id: number): Promise<CartDto> => {
  const response = await baseService.get<CartDto>(`/cart/${id}`);
  return response.data;
};

export const updateCart = async (
  id: number,
  cart: CartDto
): Promise<CartDto> => {
  const response = await baseService.put<CartDto>(`/cart/${id}`, cart);
  return response.data;
};

export const deleteCart = async (id: number): Promise<string> => {
  const response = await baseService.delete<string>(`/cart/${id}`);
  return response.data;
};
export const createCart = async (cart: CartDto): Promise<CartDto> => {
  const response = await baseService.post<CartDto>(`/cart`, cart);
  return response.data;
};
export const createCartItem = async (
  cartItem: CartItemDto
): Promise<CartItemDto> => {
  const response = await baseService.post<CartItemDto>(`/cartItem`, cartItem);
  return response.data;
};
