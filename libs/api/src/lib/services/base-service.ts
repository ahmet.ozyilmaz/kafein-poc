// libs/api/src/lib/api-service.ts
import axios from 'axios';

const instance = axios.create({
  baseURL: 'http://213.159.2.75:9002',
  headers: {
    'Content-Type': 'application/json',
  },
});
// Add this function to set the token
export const setAuthToken = (token: string | null) => {
  if (token) {
    instance.defaults.headers.common['Authorization'] = `Bearer ${token}`;
  } else {
    delete instance.defaults.headers.common['Authorization'];
  }
};

// Use an interceptor to automatically set the token for every request
instance.interceptors.request.use(
  (config) => {
    const token = localStorage.getItem('token');
    if (token) {
      config.headers['Authorization'] = `Bearer ${token}`;
    }
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default instance;
