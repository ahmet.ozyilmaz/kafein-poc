// libs/api/src/lib/customer-service.ts
import { CustomerDto } from '../../models/dtos';
import baseService from '../base-service';

export const getCustomer = async (id: number): Promise<CustomerDto> => {
  const response = await baseService.get<CustomerDto>(`/customer/${id}`);
  return response.data;
};

export const updateCustomer = async (
  id: number,
  customer: CustomerDto
): Promise<CustomerDto> => {
  const response = await baseService.put<CustomerDto>(
    `/customer/${id}`,
    customer
  );
  return response.data;
};

export const deleteCustomer = async (id: number): Promise<string> => {
  const response = await baseService.delete<string>(`/customer/${id}`);
  return response.data;
};
export const createCustomer = async (
  customer: CustomerDto
): Promise<CustomerDto> => {
  const response = await baseService.post<CustomerDto>(`/customer`, customer);
  return response.data;
};
