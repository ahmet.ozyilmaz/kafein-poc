/* eslint-disable @nrwl/nx/enforce-module-boundaries */
import React, { createContext, useContext, useEffect, useState } from 'react';
import { JwtRequest, UserDto, setAuthToken } from '@kafein/api';
import { userService } from '@kafein/api';

interface UserContextValue {
  isAuthenticated: boolean;
  user: UserDto;
  token: string;
  loginUser: (email: string, password: string) => Promise<void>;
  logoutUser: () => void;
  updateUser: (user: Partial<UserDto>) => Promise<void>;
  saveUser: (user: UserDto) => Promise<void>;
  updateToken: (token: string) => void;
}

interface UserProviderProps {
  children: React.ReactNode;
}

const defaultContextValue: UserContextValue = {
  isAuthenticated: false,
  user: {} as UserDto,
  token: '',
  loginUser: async () => {
    return;
  },
  logoutUser: () => {
    return;
  },
  updateUser: async () => {
    return;
  },
  saveUser: async () => {
    return;
  },
  updateToken: () => {
    return;
  },
};

const UserContext = createContext<UserContextValue>(defaultContextValue);

function UserProvider({ children }: UserProviderProps) {
  const [isAuthenticated, setIsAuthenticated] = useState<boolean>(false);
  const [token, setToken] = useState<string>('');
  const [user, setUser] = useState<UserDto>({} as UserDto);

  useEffect(() => {
    const storedToken = localStorage.getItem('token');
    if (storedToken) {
      setToken(storedToken);
      setIsAuthenticated(true);
      setUser({ id: Number(localStorage.getItem('id')) } as UserDto);
    }
  }, []);

  const loginUser = async (email: string, password: string) => {
    try {
      const response = await userService.createAuthenticationToken({
        username: email,
        password: password,
      } as JwtRequest);
      // auth token for axios requests
      setAuthToken(response.token);
      setIsAuthenticated(true);

      localStorage.setItem('token', response.token);
      localStorage.setItem('id', response.id.toString());

      const loggedInUser = await userService.getByUsername(email);

      setUser(loggedInUser);
    } catch (error) {
      console.log(error);
    }
  };

  const logoutUser = () => {
    localStorage.removeItem('token');
    setIsAuthenticated(false);
  };

  const updateUser = async (updatedUser: Partial<UserDto>) => {
    await userService.update(user.id, updatedUser as UserDto);
    if (user) {
      setUser({ ...user, ...updatedUser });
    }
  };

  const saveUser = async (user: UserDto) => {
    return await userService.save(user);
  };

  const updateToken = (token: string) => {
    setToken(token);
    setAuthToken(token);
    setIsAuthenticated(true);
  };

  const value: UserContextValue = {
    isAuthenticated,
    user,
    token,
    loginUser,
    logoutUser,
    updateUser,
    saveUser,
    updateToken,
  };

  return <UserContext.Provider value={value}>{children}</UserContext.Provider>;
}

const useUser = () => {
  const context = useContext(UserContext);
  if (context === undefined) {
    throw new Error('useUser must be used within a UserProvider');
  }
  return context;
};

export { UserProvider, useUser, UserContext };
