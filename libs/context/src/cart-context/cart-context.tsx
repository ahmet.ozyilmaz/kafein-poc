/* eslint-disable @nrwl/nx/enforce-module-boundaries */
// libs/context/src/lib/cart-context.tsx
import { CartItemDto, ProductDto } from '@kafein/api';
import { createContext, useContext, useState } from 'react';
interface CartContextProps {
  children: any;
}

interface CartContextValue {
  items: CartItemDto[];
  addItem: (item: ProductDto, quantity?: number) => void;
  removeItem: (itemId: number) => void;
  updateItem: (itemId: number, quantity: number) => void;
  clearCart: () => void;
}

const CartContext = createContext<CartContextValue | undefined>(undefined);

function CartProvider({ children }: CartContextProps) {
  const [items, setItems] = useState<CartItemDto[]>([]);

  const addItem = (item: ProductDto, quantity = 1) => {
    const existingItem = items.find(
      (cartItem) => cartItem.product.id === item.id
    );

    if (existingItem) {
      setItems((prevItems) =>
        prevItems.map((cartItem) =>
          cartItem.product.id === item.id
            ? { ...cartItem, quantity: cartItem.quantity + quantity }
            : cartItem
        )
      );
    } else {
      setItems((prevItems) => [
        ...prevItems,
        { product: item, quantity: Math.max(1, quantity) } as CartItemDto,
      ]);
    }
  };

  const removeItem = (productId: number) => {
    setItems((prevItems) =>
      prevItems.filter((cartItem) => cartItem.product.id !== productId)
    );
  };

  const updateItem = (productId: number, quantity: number) => {
    setItems((prevItems) =>
      prevItems.map((cartItem) =>
        cartItem.product.id === productId
          ? { ...cartItem, quantity: Math.max(1, quantity) }
          : cartItem
      )
    );
  };

  const clearCart = () => {
    setItems([]);
  };

  const value: CartContextValue = {
    items,
    addItem,
    removeItem,
    updateItem,
    clearCart,
  };

  return <CartContext.Provider value={value}>{children}</CartContext.Provider>;
}

const useCart = () => {
  const context = useContext(CartContext);
  if (context === undefined) {
    throw new Error('useCart must be used within a CartProvider');
  }
  return context;
};

export { CartProvider, useCart };
