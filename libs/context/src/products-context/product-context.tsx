/* eslint-disable @nrwl/nx/enforce-module-boundaries */
// libs/context/src/lib/product-context.tsx

import { ProductDto, getAllProducts } from '@kafein/api';
import React, { createContext, useContext, useState, useEffect } from 'react';

interface ProductProviderProps {
  children: any;
}

interface ProductContextValue {
  products: ProductDto[];
  totalPages: number;
  fetchProducts: (pageNumber: number, size?: number) => Promise<void>;
  updateProduct: (id: number, updatedProduct: Partial<ProductDto>) => void;
}

const defaultContextValue: ProductContextValue = {
  products: [],
  totalPages: 0,
  fetchProducts: async () => {},
  updateProduct: () => {},
};

const ProductContext = createContext<ProductContextValue>(defaultContextValue);

function ProductProvider({ children }: ProductProviderProps) {
  const [products, setProducts] = useState<ProductDto[]>([]);
  const [totalPages, setTotalPages] = useState<number>(0);

  const fetchProducts = async (pageNumber: number, size = 12) => {
    const response = await getAllProducts(pageNumber, size);
    setProducts(response.content);
    setTotalPages(response.totalPages);
  };

  const updateProduct = (id: number, updatedProduct: Partial<ProductDto>) => {
    setProducts((prevProducts) =>
      prevProducts.map((product) =>
        product.id === id ? { ...product, ...updatedProduct } : product
      )
    );
  };

  const value: ProductContextValue = {
    products,
    totalPages,
    fetchProducts,
    updateProduct,
  };

  return (
    <ProductContext.Provider value={value}>{children}</ProductContext.Provider>
  );
}

const useProduct = () => {
  const context = useContext(ProductContext);
  if (context === undefined) {
    throw new Error('useProduct must be used within a ProductProvider');
  }
  return context;
};

export { ProductProvider, useProduct };
