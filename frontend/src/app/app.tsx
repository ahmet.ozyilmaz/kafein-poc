/* eslint-disable @nrwl/nx/enforce-module-boundaries */
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { CartProvider, ProductProvider, UserProvider } from '@kafein/context';
import {
  Navigate,
  Route,
  BrowserRouter as Router,
  Routes,
} from 'react-router-dom';
import Footer from './components/Footer/Footer';
import Header from './components/Header/header';
import ProtectedRoute from './components/ProtectedRoute/ProtectedRoute';
import CartPage from './pages/CartPage/CartPage';
import ChangePasswordPage from './pages/ChangePassword/ChangePassword';
import CheckYourMail from './pages/CheckYourMailPage/CheckYourMailPage';
import CheckoutPage from './pages/CheckoutPage/CheckoutPage';
import EmailConfirmationPage from './pages/EmailConfirmationPage/EmailConfirmationPage';
import ForgotPasswordPage from './pages/ForgotPasswordPage/ForgotPasswordPage';
import LoginPage from './pages/LoginPage/LoginPage';
import ProductDetailPage from './pages/ProductDetailPage/ProductDetailPage';
import ProductsPage from './pages/ProductsPage/ProductsPage';
import ProfilePage from './pages/ProfilePage/ProfilePage';
import RegisterPage from './pages/RegisterPage/RegisterPage';

export function App() {
  // Replace the following line with the actual cart item count

  return (
    <UserProvider>
      <CartProvider>
        <ProductProvider>
          <Router>
            <div className="App">
              <Header />
              <Routes>
                <Route path="/" element={<Navigate to={'/products'} />} />
                <Route path="/products" element={<ProductsPage />} />
                <Route path="/cart" element={<CartPage />} />
                <Route path="/login" element={<LoginPage />} />
                <Route path="/product-detail" element={<ProductDetailPage />} />
                <Route
                  path="/forgot-password"
                  element={<ForgotPasswordPage />}
                />
                <Route path="/register" element={<RegisterPage />} />
                <Route
                  path="/update-password"
                  element={
                    <ProtectedRoute>
                      <ChangePasswordPage />
                    </ProtectedRoute>
                  }
                />
                <Route
                  path="/checkout"
                  element={
                    <ProtectedRoute>
                      <CheckoutPage />
                    </ProtectedRoute>
                  }
                />
                <Route
                  path="/profile"
                  element={
                    <ProtectedRoute>
                      <ProfilePage />
                    </ProtectedRoute>
                  }
                ></Route>
                <Route
                  path="/auth/confirm"
                  element={<EmailConfirmationPage />}
                />
                <Route path="check-your-email" element={<CheckYourMail />} />
              </Routes>
              <Footer />
            </div>
          </Router>
        </ProductProvider>
      </CartProvider>
    </UserProvider>
  );
}

export default App;
