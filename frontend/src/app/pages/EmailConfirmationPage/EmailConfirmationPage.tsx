/* eslint-disable @nrwl/nx/enforce-module-boundaries */
// apps/e-commerce-store/src/app/containers/email-confirmation/EmailConfirmation.tsx
import { userService } from '@kafein/api';
import { useUser } from '@kafein/context';
import {
  Box,
  Button,
  CircularProgress,
  Container,
  Typography,
} from '@mui/material';
import { useEffect, useState } from 'react';
import { useLocation } from 'react-router-dom';

const EmailConfirmationPage = () => {
  const { updateToken } = useUser();
  const location = useLocation();
  const [isLoading, setIsLoading] = useState(true);
  const [isConfirmed, setIsConfirmed] = useState(false);

  useEffect(() => {
    const updateTokenCallback = (token: string) => {
      updateToken(token);
    };

    const searchParams = new URLSearchParams(location.search);
    const token = searchParams.get('token');
    console.log(token);
    if (token) {
      userService.confirmUser(token).then((token) => {
        updateTokenCallback(token);
        setIsLoading(false);
        setIsConfirmed(true);
      });
    } else {
      setIsLoading(false);
      setIsConfirmed(false);
    }
  }, [location.search, updateToken]);

  const handleRedirect = () => {
    // history.push('/');
  };

  return (
    <Container>
      <Box
        display="flex"
        flexDirection="column"
        alignItems="center"
        justifyContent="center"
        minHeight="80vh"
      >
        {isLoading ? (
          <CircularProgress />
        ) : (
          <>
            <Typography variant="h4" component="h1" gutterBottom>
              {isConfirmed ? 'Email Confirmed' : 'Invalid Token'}
            </Typography>
            <Typography variant="body1" gutterBottom>
              {isConfirmed
                ? 'Your email has been successfully confirmed.'
                : 'The provided token is invalid or has expired.'}
            </Typography>
            <Typography variant="body1" gutterBottom>
              {isConfirmed
                ? 'You can now proceed to login and use our services.'
                : 'Please request a new confirmation email or contact support.'}
            </Typography>
            <Box mt={4}>
              <Button
                variant="contained"
                color="primary"
                onClick={handleRedirect}
              >
                Go to Home
              </Button>
            </Box>
          </>
        )}
      </Box>
    </Container>
  );
};

export default EmailConfirmationPage;
