// Example usage in a Login component
import styled from '@emotion/styled';
import { useUser } from '@kafein/context';
import {
  Box,
  Button,
  CircularProgress,
  Container,
  TextField,
  Typography,
} from '@mui/material';
import Link from '@mui/material/Link';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import {
  Navigate,
  Link as RouterLink,
  redirect,
  useLocation,
} from 'react-router-dom';
import * as Yup from 'yup';
const ErrorText = styled(Typography)`
  color: red;
`;

interface LoginFormValues {
  email: string;
  password: string;
}

const initialValues: LoginFormValues = {
  email: '',
  password: '',
};

const validationSchema = Yup.object({
  email: Yup.string().email('Invalid email address').required('Required'),
  password: Yup.string().required('Required'),
});

function LoginPage() {
  const { loginUser, isAuthenticated } = useUser();
  const location = useLocation(); // Get location state

  // Get the previous URL the user was trying to access, or use the home page as default
  const { from } = location.state || { from: { pathname: '/' } };
  const handleSubmit = async (values: LoginFormValues) => {
    // dumy wait for 1 second
    loginUser(values.email, values.password);
  };
  if (isAuthenticated) {
    return <Navigate to={from} />;
  } else
    return (
      <Box
        sx={{
          //center
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          height: '50vh',
        }}
      >
        <Container maxWidth="xs">
          <Typography variant="h4" align="center">
            Login
          </Typography>
          <Formik
            initialValues={initialValues}
            validationSchema={validationSchema}
            onSubmit={async (values, { setStatus }) => {
              setStatus(null);
              try {
                await handleSubmit(values);
                // Redirect or show success message
              } catch (err: any) {
                // Display server-side error
                setStatus({ error: err.message });
              }
            }}
          >
            {({ isSubmitting, status }) => (
              <Form>
                <Field
                  as={TextField}
                  fullWidth
                  margin="normal"
                  name="email"
                  label="Email"
                  type="email"
                  variant="outlined"
                  required
                />
                <ErrorMessage name="email" component={ErrorText} />
                <Field
                  as={TextField}
                  fullWidth
                  margin="normal"
                  name="password"
                  label="Password"
                  type="password"
                  variant="outlined"
                  required
                />
                <ErrorMessage name="password" component={ErrorText} />
                <Button
                  fullWidth
                  type="submit"
                  variant="contained"
                  color="primary"
                  disabled={isSubmitting}
                  startIcon={
                    isSubmitting ? <CircularProgress size={16} /> : null
                  }
                >
                  Login
                </Button>
                {status && status.error && (
                  <ErrorText align="center">{status.error}</ErrorText>
                )}
              </Form>
            )}
          </Formik>
          <Box sx={{ display: 'flex', justifyContent: 'space-between', marginTop: '40px'}}>
            <Link component={RouterLink} to="/forgot-password">
              Forgot password?
            </Link>
            <Link component={RouterLink} to="/register">
              Register
            </Link>
          </Box>
        </Container>
      </Box>
    );
}

export default LoginPage;
