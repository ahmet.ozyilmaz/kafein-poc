/* eslint-disable @nrwl/nx/enforce-module-boundaries */
// apps/e-commerce-store/src/app/containers/profile/ProfilePage.tsx
import { User, useUser } from '@kafein/context';
import { Box, Button, Container, TextField, Typography } from '@mui/material';
import { ErrorMessage, Field, Form, Formik, FormikHelpers } from 'formik';
import * as Yup from 'yup';

const validationSchema = Yup.object().shape({
  firstName: Yup.string().required('Required'),
  lastName: Yup.string().required('Required'),
  email: Yup.string().email('Invalid email').required('Required'),
  phone: Yup.string().required('Required'),
});
const defaultUser: User = {
  id: '',
  firstName: '',
  lastName: '',
  email: '',
  phone: '',
};
function ProfilePage() {
  const { user, updateUser } = useUser();

  const handleSubmit = (values: User, helpers: FormikHelpers<User>) => {
    updateUser(values);
    helpers.setSubmitting(false);
  };
  function handleChangePassword(): void {
    console.log('Change password');
  }

  return (
    <Container>
      <Box mt={4}>
        <Typography variant="h4" gutterBottom>
          {user.name}
        </Typography>
        <Formik
          initialValues={
            // merge default values with user values
            // so that we don't have to check for undefined values
            { ...defaultUser, ...user }
          }
          validationSchema={validationSchema}
          onSubmit={handleSubmit}
        >
          {({ isSubmitting, touched, errors, isValid }) => (
            <Form>
              <Box mb={2}>
                <Field
                  name="firstName"
                  as={TextField}
                  fullWidth
                  label="First Name"
                  variant="outlined"
                  helperText={<ErrorMessage name="firstName" />}
                  error={touched.firstName && !!errors.firstName}
                />
              </Box>
              <Box mb={2}>
                <Field
                  name="lastName"
                  as={TextField}
                  fullWidth
                  label="Last Name"
                  variant="outlined"
                  helperText={<ErrorMessage name="lastName" />}
                  error={touched.lastName && !!errors.lastName}
                />
              </Box>
              <Box mb={2}>
                <Field
                  name="email"
                  as={TextField}
                  fullWidth
                  label="Email"
                  variant="outlined"
                  helperText={<ErrorMessage name="email" />}
                  error={touched.email && !!errors.email}
                />
              </Box>
              <Box mb={2}>
                <Field
                  name="phone"
                  as={TextField}
                  fullWidth
                  label="Phone"
                  variant="outlined"
                  helperText={<ErrorMessage name="phone" />}
                  error={touched.phone && !!errors.phone}
                />
              </Box>

              <Box display="flex" justifyContent="flex-end">
                <Button
                  variant="contained"
                  color="primary"
                  type="submit"
                  //disable when invalid
                  disabled={
                    isSubmitting ||
                    !isValid ||
                    Object.keys(touched).length === 0
                  }
                >
                  Update Profile
                </Button>
              </Box>
            </Form>
          )}
        </Formik>
      </Box>
    </Container>
  );
}

export default ProfilePage;
