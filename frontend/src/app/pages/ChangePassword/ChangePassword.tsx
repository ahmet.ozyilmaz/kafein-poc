// apps/e-commerce-store/src/app/containers/change-password/ChangePassword.tsx
import React, { useState } from 'react';
import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import {
  Container,
  Typography,
  Box,
  Grid,
  TextField,
  Button,
  FormControl,
  FormHelperText,
  IconButton,
  InputAdornment,
  Alert,
  Snackbar,
} from '@mui/material';
import { userService } from '@kafein/api';
import { Visibility, VisibilityOff } from '@mui/icons-material';
import { useNavigate } from 'react-router-dom';

interface ChangePasswordFormValues {
  oldPassword: string;
  newPassword: string;
  confirmPassword: string;
}

const initialValues: ChangePasswordFormValues = {
  oldPassword: '',
  newPassword: '',
  confirmPassword: '',
};

const validationSchema = Yup.object().shape({
  oldPassword: Yup.string()
    .min(8, 'Password must be at least 8 characters')
    .required('Old password is required'),
  newPassword: Yup.string()
    .min(8, 'Password must be at least 8 characters')
    .required('New password is required'),
  confirmPassword: Yup.string()
    .oneOf([Yup.ref('newPassword')], 'Passwords must match')
    .required('Confirm password is required'),
});

const ChangePasswordPage = () => {
  const [openSnackbar, setOpenSnackbar] = useState(false);

  const navigate = useNavigate();
  const handleSubmit = (values: ChangePasswordFormValues) => {
    userService
      .resetPassword({
        oldPassword: values.oldPassword,
        newPassword: values.newPassword,
      })
      .then(() => {
        setTimeout(() => {
          setOpenSnackbar(true);

          // Navigate to '/' path after 1 second to allow the Snackbar to be visible
          setTimeout(() => {
            navigate('/');
          }, 1000);
        }, 500);
      });
  };
  const [showPassword, setShowPassword] = useState({
    oldPassword: false,
    newPassword: false,
    confirmPassword: false,
  });

  const handleCloseSnackbar = () => {
    setOpenSnackbar(false);
  };

  const handleClickShowPassword = (field: keyof typeof showPassword) => {
    setShowPassword({ ...showPassword, [field]: !showPassword[field] });
  };

  const handleMouseDownPassword = (
    event: React.MouseEvent<HTMLButtonElement>
  ) => {
    event.preventDefault();
  };

  return (
    <Box
      sx={{
        //center
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '50vh',
      }}
    >
      <Snackbar
        open={openSnackbar}
        autoHideDuration={6000}
        onClose={handleCloseSnackbar}
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }}
      >
        <Alert
          onClose={handleCloseSnackbar}
          severity="success"
          sx={{ width: '100%' }}
        >
          Password updated successfully!
        </Alert>
      </Snackbar>
      <Container>
        <Box mt={4}>
          <Typography variant="h4" component="h1" align="center">
            Change Password
          </Typography>
        </Box>
        <Box mt={4}>
          <Grid container justifyContent="center">
            <Grid item xs={12} sm={8} md={6} lg={4}>
              <Formik
                initialValues={initialValues}
                validationSchema={validationSchema}
                onSubmit={handleSubmit}
              >
                {({ errors, touched, setFieldValue }) => (
                  <Form>
                    <FormControl fullWidth margin="normal">
                      <Field
                        as={TextField}
                        name="oldPassword"
                        label="Old Password"
                        type={showPassword.oldPassword ? 'text' : 'password'}
                        variant="outlined"
                        error={touched.oldPassword && !!errors.oldPassword}
                        helperText={touched.oldPassword && errors.oldPassword}
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="end">
                              <IconButton
                                onClick={() =>
                                  handleClickShowPassword('oldPassword')
                                }
                                onMouseDown={handleMouseDownPassword}
                                edge="end"
                              >
                                {showPassword.oldPassword ? (
                                  <Visibility />
                                ) : (
                                  <VisibilityOff />
                                )}
                              </IconButton>
                            </InputAdornment>
                          ),
                        }}
                      />
                    </FormControl>
                    <FormControl fullWidth margin="normal">
                      <Field
                        as={TextField}
                        name="newPassword"
                        label="New Password"
                        type={showPassword.newPassword ? 'text' : 'password'}
                        variant="outlined"
                        error={touched.newPassword && !!errors.newPassword}
                        helperText={touched.newPassword && errors.newPassword}
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="end">
                              <IconButton
                                onClick={() =>
                                  handleClickShowPassword('newPassword')
                                }
                                onMouseDown={handleMouseDownPassword}
                                edge="end"
                              >
                                {showPassword.newPassword ? (
                                  <Visibility />
                                ) : (
                                  <VisibilityOff />
                                )}
                              </IconButton>
                            </InputAdornment>
                          ),
                        }}
                      />
                    </FormControl>
                    <FormControl fullWidth margin="normal">
                      <Field
                        as={TextField}
                        name="confirmPassword"
                        label="Confirm Password"
                        type={
                          showPassword.confirmPassword ? 'text' : 'password'
                        }
                        variant="outlined"
                        error={
                          touched.confirmPassword && !!errors.confirmPassword
                        }
                        helperText={
                          touched.confirmPassword && errors.confirmPassword
                        }
                        InputProps={{
                          endAdornment: (
                            <InputAdornment position="end">
                              <IconButton
                                onClick={() =>
                                  handleClickShowPassword('confirmPassword')
                                }
                                onMouseDown={handleMouseDownPassword}
                                edge="end"
                              >
                                {showPassword.confirmPassword ? (
                                  <Visibility />
                                ) : (
                                  <VisibilityOff />
                                )}
                              </IconButton>
                            </InputAdornment>
                          ),
                        }}
                      />
                    </FormControl>
                    <Box mt={2}>
                      <Button
                        type="submit"
                        variant="contained"
                        color="primary"
                        fullWidth
                      >
                        Change Password
                      </Button>
                    </Box>
                  </Form>
                )}
              </Formik>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </Box>
  );
};

export default ChangePasswordPage;
