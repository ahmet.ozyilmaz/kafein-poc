/* eslint-disable @nrwl/nx/enforce-module-boundaries */
// apps/e-commerce-store/src/app/containers/products/Products.tsx
import { ProductDto } from '@kafein/api';
import { useCart, useProduct } from '@kafein/context';
import {
  Box,
  Button,
  Card,
  CardActionArea,
  CardContent,
  CardMedia,
  Container,
  Grid,
  Pagination,
  Typography,
} from '@mui/material';
import React, { useCallback, useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';

function ProductsPage() {
  const { products, totalPages, fetchProducts } = useProduct();
  const { addItem } = useCart();
  const [currentPage, setCurrentPage] = useState<number>(1);
  //callback
  const fetchProductsCallback = useCallback(
    (currentPage: number) => fetchProducts(currentPage),
    []
  );
  useEffect(() => {
    (async () => {
      await fetchProductsCallback(currentPage);
    })();
  }, [currentPage, fetchProductsCallback]);
  const handlePageChange = (
    event: React.ChangeEvent<unknown>,
    value: number
  ) => {
    setCurrentPage(value);
  };

  const handleAddToCart = (product: ProductDto) => {
    addItem(product);
  };

  const navigate = useNavigate();

  const handleClick = (index: any) => {
    navigate('/product-detail', { state: { product: products[index] } });
  };

  const productCards = products.map((product, index) => (
    <Grid key={index} item xs={12} sm={6} md={4} lg={3}>
      <Card>
        <CardActionArea onClick={() => handleClick(index)}>
          <CardMedia
            component="img"
            height="200"
            image={
              'https://ffo3gv1cf3ir.merlincdn.net/SiteAssets/Hakkimizda/genel-bakis/logolarimiz/TURKCELL_YATAY_DISI_LOGO.png'
            }
            alt={product.name}
          />
          <CardContent>
            <Typography gutterBottom variant="h5" component="div">
              {product.name}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              {product.description}
            </Typography>
            <Typography variant="h6" component="div">
              ${product.price.toFixed(2)}
            </Typography>
          </CardContent>
        </CardActionArea>
        <Box m={2}>
          <Button
            variant="contained"
            color="primary"
            fullWidth
            onClick={() => handleAddToCart(product)}
          >
            Add to Cart
          </Button>
        </Box>
      </Card>
    </Grid>
  ));
  return (
    <Container sx={{ marginBottom: '400px' }}>
      <Box mt={4}>
        <Grid container spacing={4}>
          {productCards}
        </Grid>
      </Box>
      <Box display="flex" justifyContent="center" mt={4}>
        <Pagination
          count={totalPages}
          page={currentPage}
          onChange={handlePageChange}
        />
      </Box>
    </Container>
  );
}

export default ProductsPage;
