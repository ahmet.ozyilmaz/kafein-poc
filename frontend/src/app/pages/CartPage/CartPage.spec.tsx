import { render } from '@testing-library/react';

import CartPage from './CartPage';

describe('CartPage', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<CartPage />);
    expect(baseElement).toBeTruthy();
  });
});
