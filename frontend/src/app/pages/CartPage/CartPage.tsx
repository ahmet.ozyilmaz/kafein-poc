/* eslint-disable @nrwl/nx/enforce-module-boundaries */
import { useCart } from '@kafein/context';
import { Delete as DeleteIcon } from '@mui/icons-material';
import {
  Box,
  Button,
  Container,
  IconButton,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  TextField,
  Typography,
} from '@mui/material';
import { useNavigate } from 'react-router-dom';

/* eslint-disable-next-line */
export interface CartPageProps {}

export function CartPage(props: CartPageProps) {
  const { items, removeItem, updateItem } = useCart();
  const navigate = useNavigate();

  const handleRemoveItem = (productId: number) => {
    removeItem(productId);
  };

  const handleUpdateItem = (productId: number, quantity: number) => {
    updateItem(productId, quantity);
  };

  const handleCheckout = () => {
    navigate('/checkout');
  };

  return (
    <Container>
      <Box mt={4}>
        <Typography variant="h4" gutterBottom>
          Your Cart
        </Typography>
        <TableContainer>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Product</TableCell>
                <TableCell>Description</TableCell>
                <TableCell align="right">Price</TableCell>
                <TableCell align="right">Quantity</TableCell>
                <TableCell align="right">Total</TableCell>
                <TableCell align="right">Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {items.map((item) => (
                <TableRow key={item.id}>
                  <TableCell>
                    <img
                      src={
                        'https://ffo3gv1cf3ir.merlincdn.net/SiteAssets/Hakkimizda/genel-bakis/logolarimiz/TURKCELL_YATAY_DISI_LOGO.png'
                      }
                      alt="turkcell"
                      width="100"
                    />
                  </TableCell>
                  <TableCell>
                    <Typography variant="subtitle1">
                      {item.product.name}
                    </Typography>
                  </TableCell>
                  <TableCell align="right">
                    ${item.product.price.toFixed(2)}
                  </TableCell>
                  <TableCell align="right">
                    <TextField
                      type="number"
                      InputProps={{ inputProps: { min: 1 } }}
                      value={item.quantity}
                      onChange={(e) =>
                        handleUpdateItem(
                          item.product.id,
                          parseInt(e.target.value, 10)
                        )
                      }
                    />
                  </TableCell>
                  <TableCell align="right">
                    ${(item.product.price * item.quantity).toFixed(2)}
                  </TableCell>
                  <TableCell align="right">
                    <IconButton
                      color="error"
                      onClick={() => handleRemoveItem(item.product.id)}
                    >
                      <DeleteIcon />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <Box mt={4} display="flex" justifyContent="flex-end">
          <Button variant="contained" color="primary" onClick={handleCheckout}>
            Checkout
          </Button>
        </Box>
      </Box>
    </Container>
  );
}

export default CartPage;
