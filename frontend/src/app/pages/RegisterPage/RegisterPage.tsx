import { UserDto } from '@kafein/api';
import { useUser } from '@kafein/context';
import {
  Button,
  Container,
  TextField,
  Typography,
  styled,
} from '@mui/material';
import { CircularProgress } from '@mui/material';

import { Box } from '@mui/system';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import { useNavigate } from 'react-router-dom';
import * as Yup from 'yup';
const ErrorText = styled(Typography)`
  color: red;
`;

interface RegisterFormValues {
  email: string;
  password: string;
  confirmPassword: string;
}

const initialValues: RegisterFormValues = {
  email: '',
  password: '',
  confirmPassword: '',
};

const validationSchema = Yup.object({
  email: Yup.string().email('Invalid email address').required('Required'),
  password: Yup.string()
    .min(6, 'Password must be at least 6 characters')
    .required('Required'),
  confirmPassword: Yup.string()
    .oneOf([Yup.ref('password')], 'Passwords must match')
    .required('Required'),
});
function RegisterPage() {
  const navigate = useNavigate();
  const { saveUser } = useUser();
  function handleSubmit(e: RegisterFormValues) {
    const user = saveUser({
      username: e.email,
      email: e.email,
      password: e.password,
    } as UserDto).then((res) => {
      navigate('/check-your-email');
    });
  }

  return (
    <Box
      sx={{
        //center
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '50vh',
      }}
    >
      <Container maxWidth="xs">
        <Typography variant="h4" align="center">
          Register
        </Typography>
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={async (values, { setStatus }) => {
            setStatus(null);
            try {
              await handleSubmit(values);
              // Redirect or show success message
            } catch (err: any) {
              // Display server-side error
              setStatus({ error: err.message });
            }
          }}
        >
          {({ isSubmitting, status }) => (
            <Form>
              <Field
                as={TextField}
                fullWidth
                margin="normal"
                name="email"
                label="Email"
                type="email"
                variant="outlined"
                required
              />
              <ErrorMessage name="email" component={ErrorText} />
              <Field
                as={TextField}
                fullWidth
                margin="normal"
                name="password"
                label="Password"
                type="password"
                variant="outlined"
                required
              />
              <ErrorMessage name="password" component={ErrorText} />
              <Field
                as={TextField}
                fullWidth
                margin="normal"
                name="confirmPassword"
                label="Confirm Password"
                type="password"
                variant="outlined"
                required
              />
              <ErrorMessage name="confirmPassword" component={ErrorText} />

              <Button
                fullWidth
                type="submit"
                variant="contained"
                color="primary"
                disabled={isSubmitting}
              >
                {isSubmitting ? <CircularProgress size={16} /> : 'Register'}
              </Button>
              {status && status.error && (
                <ErrorText align="center">{status.error}</ErrorText>
              )}
            </Form>
          )}
        </Formik>
      </Container>
    </Box>
  );
}

export default RegisterPage;
