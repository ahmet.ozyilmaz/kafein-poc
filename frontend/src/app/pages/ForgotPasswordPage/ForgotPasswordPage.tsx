// src/components/ForgotPassword.tsx
import {
  Button,
  CircularProgress,
  Container,
  TextField,
  Typography,
  styled,
} from '@mui/material';
import { Box } from '@mui/system';
import { ErrorMessage, Field, Form, Formik } from 'formik';
import * as Yup from 'yup';

const ErrorText = styled(Typography)`
  color: red;
`;

interface ForgotPasswordFormValues {
  email: string;
}

const initialValues: ForgotPasswordFormValues = {
  email: '',
};

const validationSchema = Yup.object({
  email: Yup.string().email('Invalid email address').required('Required'),
});

function ForgotPasswordPage() {
  function handleSubmit(e: ForgotPasswordFormValues) {
    console.dir('forgot password');
    // dumy wait for 1 second
    return new Promise((resolve) => setTimeout(resolve, 1000));

    // Implement your forgot password logic here
  }

  return (
    <Box
      sx={{
        //center
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '50vh',
      }}
    >
      <Container maxWidth="xs">
        <Typography variant="h4" align="center">
          Forgot Password
        </Typography>
        <Formik
          initialValues={initialValues}
          validationSchema={validationSchema}
          onSubmit={async (values, { setStatus }) => {
            setStatus(null);
            try {
              await handleSubmit(values);
              // Redirect or show success message
            } catch (err: any) {
              // Display server-side error
              setStatus({ error: err.message });
            }
          }}
        >
          {({ isSubmitting, status }) => (
            <Form>
              <Field
                as={TextField}
                ß
                fullWidth
                margin="normal"
                name="email"
                label="Email"
                type="email"
                variant="outlined"
                required
              />
              <ErrorMessage name="email" component={ErrorText} />
              <Button
                fullWidth
                type="submit"
                variant="contained"
                color="primary"
                disabled={isSubmitting}
                startIcon={isSubmitting ? <CircularProgress size={16} /> : null}
              >
                Reset Password
              </Button>
              {status && status.error && (
                <ErrorText align="center">{status.error}</ErrorText>
              )}
            </Form>
          )}
        </Formik>
      </Container>
    </Box>
  );
}

export default ForgotPasswordPage;
