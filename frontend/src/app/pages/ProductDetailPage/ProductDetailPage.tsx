/* eslint-disable @nrwl/nx/enforce-module-boundaries */
import { ProductDto } from '@kafein/api';
import { useCart } from '@kafein/context';
import {
    Box,
    Button,
    Card,
    CardContent,
    CardMedia,
    Container,
    Typography
} from '@mui/material';
import { useLocation } from 'react-router-dom';

function ProductDetailPage() {
  const { addItem } = useCart();

  // product from route state
  const { state } = useLocation();
ß  const handleAddToCart = (product: ProductDto) => {
    addItem(product);
  };

  const product: ProductDto = state.product;
  return (
    <Container
      sx={{
        marginTop: '90px',
      }}
    >
      <Card sx={{ display: 'flex', margin: '100px' }}>
        <CardMedia
          sx={{ margin: '30px' }}
          component="img"
          height="200"
          image="https://ffo3gv1cf3ir.merlincdn.net/SiteAssets/Hakkimizda/genel-bakis/logolarimiz/TURKCELL_YATAY_DISI_LOGO.png"
          alt={product?.name}
        />
        <Box sx={{ display: 'flex', flexDirection: 'column' }}>
          <CardContent sx={{ flex: '1 0 auto' }}>
            <Typography gutterBottom variant="h5" component="div">
              {product?.name}
            </Typography>
            <Typography variant="body2" color="text.secondary">
              {product?.description}
            </Typography>
            <Typography variant="h6" component="div">
              ${product?.price?.toFixed(2)}
            </Typography>
          </CardContent>
          <Box>
            <Button
              onClick={() => handleAddToCart(product)}
              sx={{ width: '300px', float: 'right', margin: '20px' }}
              variant="contained"
              color="primary"
              fullWidth
            >
              Add to Cart
            </Button>
          </Box>
        </Box>
      </Card>
    </Container>
  );
}

export default ProductDetailPage;
