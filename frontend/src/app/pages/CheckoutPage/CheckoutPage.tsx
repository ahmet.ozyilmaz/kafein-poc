/* eslint-disable @nrwl/nx/enforce-module-boundaries */
// apps/e-commerce-store/src/app/containers/checkout/Checkout.tsx
import React from 'react';
import {
  Container,
  Typography,
  Box,
  List,
  ListItem,
  ListItemAvatar,
  Avatar,
  ListItemText,
  ListItemSecondaryAction,
  Button,
  Divider,
  Grid,
  TextField,
} from '@mui/material';
import { useCart } from '@kafein/context';

const CheckoutPage = () => {
  const { items, removeItem, updateItem } = useCart();

  const handleQuantityChange = (itemId: number, quantity: number) => {
    updateItem(itemId, isNaN(quantity) ? 1 : quantity);
  };

  const handleRemoveItem = (itemId: number) => {
    removeItem(itemId);
  };

  const total = items.reduce(
    (sum, item) => sum + item.product.price * item.quantity,
    0
  );

  return (
    <Container>
      <Box mt={4}>
        <Typography variant="h4" component="h1">
          Checkout
        </Typography>
      </Box>
      <Box mt={4}>
        <Grid container spacing={4}>
          <Grid item xs={12} md={8}>
            <Typography variant="h6" component="h2">
              Cart Items
            </Typography>
            <List>
              {items.map((item, index) => (
                <React.Fragment key={item.id}>
                  <ListItem>
                    <ListItemAvatar>
                      <Avatar
                        alt={item.product.name}
                        src="https://ffo3gv1cf3ir.merlincdn.net/SiteAssets/Hakkimizda/genel-bakis/logolarimiz/TURKCELL_YATAY_DISI_LOGO.png"
                      />
                    </ListItemAvatar>
                    <ListItemText
                      primary={item.product.name}
                      secondary={`$${item.product.price.toFixed(2)} x ${
                        item.quantity
                      }`}
                    />
                    <ListItemSecondaryAction>
                      <TextField
                        variant="outlined"
                        type="number"
                        size="small"
                        value={item.quantity}
                        onChange={(e) =>
                          handleQuantityChange(
                            item.product.id,
                            parseInt(e.target.value, 10)
                          )
                        }
                      />
                      <Button
                        color="secondary"
                        onClick={() => handleRemoveItem(item.id)}
                      >
                        Remove
                      </Button>
                    </ListItemSecondaryAction>
                  </ListItem>
                  {index < items.length - 1 && <Divider />}
                </React.Fragment>
              ))}
            </List>
          </Grid>
          <Grid item xs={12} md={4}>
            <Typography variant="h6" component="h2">
              Order Summary
            </Typography>
            <List>
              <ListItem>
                <ListItemText primary="Total" />
                <ListItemSecondaryAction>
                  <Typography variant="h6" component="span">
                    ${total.toFixed(2)}
                  </Typography>
                </ListItemSecondaryAction>
              </ListItem>
              <ListItem>
                <Button variant="contained" color="primary" fullWidth>
                  Place Order
                </Button>
              </ListItem>
            </List>
          </Grid>
        </Grid>
      </Box>
    </Container>
  );
};

export default CheckoutPage;
