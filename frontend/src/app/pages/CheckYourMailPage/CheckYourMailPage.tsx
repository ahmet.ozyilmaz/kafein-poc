// apps/e-commerce-store/src/app/containers/check-your-mail/CheckYourMail.tsx
import React from 'react';
import { Container, Box, Typography, Button } from '@mui/material';
import MailOutlineIcon from '@mui/icons-material/MailOutline';

const CheckYourMail = () => {
  const handleRedirect = () => {
    // history.push('/');
  };

  return (
    <Container>
      <Box
        display="flex"
        flexDirection="column"
        alignItems="center"
        justifyContent="center"
        minHeight="80vh"
      >
        <MailOutlineIcon color="primary" style={{ fontSize: 100 }} />
        <Typography variant="h4" component="h1" gutterBottom>
          Check Your Mail
        </Typography>
        <Typography variant="body1" gutterBottom>
          We've sent an email confirmation link to your email address. Please
          check your inbox and follow the instructions to confirm your email.
        </Typography>
        <Box mt={4}>
          <Button variant="contained" color="primary" onClick={handleRedirect}>
            Go to Home
          </Button>
        </Box>
      </Box>
    </Container>
  );
};

export default CheckYourMail;
