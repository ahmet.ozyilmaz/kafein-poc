/* eslint-disable @nrwl/nx/enforce-module-boundaries */
import styled from '@emotion/styled';
import { useCart, useUser } from '@kafein/context';
import { ArrowBack } from '@mui/icons-material';
import AccountCircle from '@mui/icons-material/AccountCircle';
import ShoppingCartIcon from '@mui/icons-material/ShoppingCart';
import {
  AppBar,
  Badge,
  IconButton,
  Link,
  Menu,
  MenuItem,
  Toolbar,
  Typography,
} from '@mui/material';
import { useState } from 'react';
import { Link as RouterLink, useNavigate } from 'react-router-dom';

const Title = styled(Typography)`
  flex-grow: 1;
`;
export function Header() {
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const { isAuthenticated, logoutUser } = useUser();
  const { items: cartItems } = useCart();
  // useUser
  const handleMenuOpen = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  const navigate = useNavigate();

  const handleLogout = () => {
    logoutUser();
    handleMenuClose();
    navigate('/products');
  };

  function handleBackClick(): void {
    navigate(-1);
  }

  return (
    <AppBar position="static">
      <Toolbar>
        <IconButton
          edge="start"
          onClick={handleBackClick}
          color="inherit"
          aria-label="back button"
        >
          <ArrowBack />
        </IconButton>
        <Title variant="h6">
          <RouterLink to="/" style={{ color: 'white', textDecoration: 'none' }}>
            Kafein Store
          </RouterLink>
        </Title>
        <RouterLink
          to="/cart"
          style={{ textDecoration: 'none', color: 'inherit' }}
        >
          <IconButton edge="end" color="inherit" aria-label="shopping cart">
            <Badge badgeContent={cartItems.length} color="secondary">
              <ShoppingCartIcon />
            </Badge>
          </IconButton>
        </RouterLink>

        <IconButton
          onClick={handleMenuOpen}
          edge="end"
          color="inherit"
          aria-label="user profile"
        >
          <AccountCircle />
        </IconButton>
        <Menu
          anchorEl={anchorEl}
          open={Boolean(anchorEl)}
          onClose={handleMenuClose}
          onClick={handleMenuClose}
          PaperProps={{
            elevation: 3,
          }}
        >
          {isAuthenticated && (
            <Link component={RouterLink} to="/profile">
              <MenuItem key="profile">Profile</MenuItem>
            </Link>
          )}

          {isAuthenticated && (
            // logout
            <Link component={RouterLink} to="/logout">
              <MenuItem key="logout" onClick={handleLogout}>
                Logout
              </MenuItem>
            </Link>
          )}
          {/* change password */}
          {isAuthenticated && (
            <Link component={RouterLink} to="/update-password">
              <MenuItem key="changePassword">Change Password</MenuItem>
            </Link>
          )}
          {!isAuthenticated && (
            <Link component={RouterLink} to="/Login">
              <MenuItem key="login">Login</MenuItem>
            </Link>
          )}
        </Menu>
      </Toolbar>
    </AppBar>
  );
}

export default Header;
