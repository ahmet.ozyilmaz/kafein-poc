import { Box, Container, Grid, Typography } from '@mui/material';
import { Link } from 'react-router-dom';

/* eslint-disable-next-line */
export interface FooterProps {}

export function Footer(props: FooterProps) {
  return (
    <Box
      component="footer"
      //fixed at bottom
      sx={{
        backgroundColor: 'primary.main',
        color: 'white',
        py: 4,
        position: 'fixed',
        bottom: 0,
        left: 0,
        right: 0,
        zIndex: 1000,
      }}
    >
      <Container maxWidth="lg">
        <Grid container spacing={4}>
          <Grid item xs={12} sm={6} md={4}>
            <Typography variant="h6" gutterBottom>
              About Us
            </Typography>
            <Typography>
              We are a leading e-commerce platform providing a wide range of
              products to our customers at affordable prices.
            </Typography>
          </Grid>
          <Grid item xs={12} sm={6} md={4}>
            <Typography variant="h6" gutterBottom>
              Quick Links
            </Typography>
            <ul style={{ listStyle: 'none', margin: 0, padding: 0 }}>
              <li>
                <Link to="/" color="inherit">
                  Home
                </Link>
              </li>
              <li>
                <Link to="products" color="inherit">
                  Products
                </Link>
              </li>
              <li> 
                <Link to="#" color="inherit">
                  Contact Us
                </Link>
              </li>
              <li>
                <Link to="#" color="inherit">
                  Privacy Policy
                </Link>
              </li>
            </ul>
          </Grid>
          <Grid item xs={12} sm={6} md={4}>
            <Typography variant="h6" gutterBottom>
              Connect with us
            </Typography>
            <Typography>
              Stay connected with us on our social media platforms for the
              latest updates and promotions.
            </Typography>
          </Grid>
        </Grid>
        <Box sx={{ borderTop: '1px solid white', mt: 4, pt: 2 }}>
          <Typography variant="body2" align="center">
            &copy; {new Date().getFullYear()} My E-commerce Store. All rights
            reserved.
          </Typography>
        </Box>
      </Container>
    </Box>
  );
}

export default Footer;
