package org.kafein.poc.userservice.repository;

import org.kafein.poc.userservice.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends BaseRepository<User> {
    Optional<User> getByUsername(String username);

    Optional<User> getByEmail(String email);
}
