package org.kafein.poc.userservice.service;

import io.jsonwebtoken.Claims;
import org.kafein.poc.userservice.entity.BaseEntity;
import org.kafein.poc.userservice.exception.NotFoundException;
import org.kafein.poc.userservice.repository.BaseRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;

import java.lang.reflect.ParameterizedType;
import java.util.Date;
import java.util.List;
import java.util.Optional;


public abstract class AbstractService<ENTITY extends BaseEntity> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractService.class);
	
	public ENTITY get(Long id) {
		Optional<ENTITY> optionalENTITY = getRepository().findById(id);
		if (optionalENTITY.isPresent()) {
			return optionalENTITY.get();
		} else {
			throw new NotFoundException("this is not found " + id);
		}
	}
	
	@Transactional
	public ENTITY save(ENTITY entity) {
		entity.setEditedBy(getCurrentUserId());
		entity.setCreationDate(new Date());
		verifySave(entity);
		entity = persist(entity);
		afterSave(entity);
		return entity;
	}
	
	protected void afterSave(ENTITY entity) {
	
	}
	
	public ENTITY persist(ENTITY entity) {
		try {
			entity.setCreatedBy(getCurrentUserId());
		} catch (Exception e) {
			LOGGER.warn("getCurrentUserId could not fetch for entity PERSIST : {}", entity);
		}
		entity.setLastUpdateDate(new Date());
		return getRepository().save(entity);
	}
	
	public Long getCurrentUserId() {
		var authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication.getPrincipal().equals("anonymousUser")) {
			return null;
		}
		return Long.parseLong(((Claims) authentication.getCredentials()).get("user_id").toString());
	}
	
	
	public String getCurrentUserName() {
		var authentication = SecurityContextHolder.getContext().getAuthentication();
		if(authentication.getPrincipal().equals("anonymousUser")){
			return null;
		}
		return ((Claims) authentication.getCredentials()).get("username").toString();
	}
	
	
	protected void verifySave(ENTITY entity) {
	}
	
	protected void verifyPut(ENTITY forSave, ENTITY real) {
	}
	
	@Transactional
	public ENTITY put(Long id, ENTITY forSave) {
		ENTITY theReal = get(id);
		forSave.setId(theReal.getId());
		forSave.setCreationDate(theReal.getCreationDate());
		forSave.setEditedBy(getCurrentUserId());
		forSave.setLastUpdateDate(new Date());
		
		verifyPut(forSave, theReal);
		forSave = persist(forSave);
		afterPut(theReal, forSave);
		return forSave;
	}
	
	protected void afterPut(ENTITY oldEntity, ENTITY newEntity) {
	}
	
	public List<ENTITY> getAll() {
		return getRepository().findAll();
	}
	
	public void delete(Long id) {
		get(id);
		getRepository().deleteById(id);
	}
	
	public Page<ENTITY> allByPage(Pageable pageable) {
		return getRepository().findAll(pageable);
	}
	
	private Class<ENTITY> getEntityClass() {
		return (Class<ENTITY>)
				((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
	}
	
	public abstract BaseRepository<ENTITY> getRepository();
}
