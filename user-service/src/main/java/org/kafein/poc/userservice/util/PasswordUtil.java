package org.kafein.poc.userservice.util;

import org.apache.commons.lang.RandomStringUtils;

public class PasswordUtil {

    public static String generatePassword() {
        final String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        return RandomStringUtils.random(10, chars);
    }
}
