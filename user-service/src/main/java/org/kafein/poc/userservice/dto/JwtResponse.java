package org.kafein.poc.userservice.dto;

import lombok.Data;

@Data
public class JwtResponse {
    private final String token;
}
