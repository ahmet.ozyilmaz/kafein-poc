package org.kafein.poc.userservice.mapper;

import org.kafein.poc.userservice.dto.BaseEntityDto;
import org.kafein.poc.userservice.entity.BaseEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface AbstractMapper<ENTITY extends BaseEntity, DTO extends BaseEntityDto> {
	
	ENTITY toEntity(DTO dto);
	
	DTO toDto(ENTITY entity);
	
	List<ENTITY> toEntity(List<DTO> dtoList);
	
	List<DTO> toDto(List<ENTITY> entityList);
	
	default Page<DTO> toDto(Page<ENTITY> entities) {
		List<DTO> personDTOList = toDto(entities.getContent());
		Pageable pageable = PageRequest.of(entities.getNumber(), entities.getSize(), entities.getSort());
		return new PageImpl<>(personDTOList, pageable, entities.getTotalElements());
	}
}
