package org.kafein.poc.userservice.controller;

import lombok.RequiredArgsConstructor;
import org.kafein.poc.userservice.dto.JwtRequest;
import org.kafein.poc.userservice.dto.JwtResponse;
import org.kafein.poc.userservice.dto.UserDto;
import org.kafein.poc.userservice.service.JwtUserDetailsService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class JwtAuthenticationController {

    private final JwtUserDetailsService jwtUserDetailsService;

    @PostMapping(value = "/get-token")
    public JwtResponse createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) {
        final String token = jwtUserDetailsService.generateToken(authenticationRequest);
        return new JwtResponse(token);
    }

    @PostMapping(value = "/forget-password")
    public void forgetPassword(@RequestParam("email") String email) {
        jwtUserDetailsService.sendMailToForgotPassword(email);
    }

    @PostMapping(value = "/save-user")
    public void saveUser(@RequestBody UserDto userDto) {
        jwtUserDetailsService.saveUser(userDto);
    }

    @PostMapping(value = "confirm-user")
    public String confirmUser(@RequestParam("token") String token) {
        return jwtUserDetailsService.confirmUser(token);
    }
}
