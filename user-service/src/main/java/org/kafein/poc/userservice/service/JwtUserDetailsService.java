package org.kafein.poc.userservice.service;

import java.util.HashMap;
import java.util.Map;

import io.jsonwebtoken.Claims;
import lombok.RequiredArgsConstructor;
import org.kafein.poc.userservice.dto.JwtRequest;
import org.kafein.poc.userservice.dto.UserDto;
import org.kafein.poc.userservice.entity.User;
import org.kafein.poc.userservice.exception.BadRequestException;
import org.kafein.poc.userservice.mapper.UserMapper;
import org.kafein.poc.userservice.service.mail.MailService;
import org.kafein.poc.userservice.util.PasswordUtil;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class JwtUserDetailsService {

    private final UserService userService;

    private final MailService mailService;

    private final UserMapper userMapper;
    private final JwtTokenUtil jwtTokenUtil;
    public static PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();;

    public String generateToken(JwtRequest request) {
        UserDto userInDb = userService.getByUsername(request.getUsername());
        if(!passwordEncoder.matches(request.getPassword(), userInDb.getPassword())) {
            throw new RuntimeException("Not found");
        }
        Map<String, Object> claims = new HashMap<>();
        claims.put("username", request.getUsername());
        claims.put("id", userInDb.getId());
        return jwtTokenUtil.doGenerateToken(claims, request.getUsername());
    }

    public void sendMailToForgotPassword(String email) {
        UserDto userInDb = userService.getByEmail(email);
        if(userInDb == null) {
            throw new RuntimeException("Not found");
        }
        String firstPassword = PasswordUtil.generatePassword();
        userInDb.setPassword(passwordEncoder.encode(firstPassword));
        userService.save(userMapper.toEntity(userInDb));
        mailService.sendEmail(email, "Kafein POC", "Your new password is: " + firstPassword);
    }

    public void saveUser(UserDto userDto) {
        var user = userMapper.toEntity(userDto);
        user.setPassword(passwordEncoder.encode(userDto.getPassword()));
        var token = jwtTokenUtil.doGenerateTokenForTwoMinutes(new HashMap<>(), user.getUsername());
        user.setActivationCode(token);
        user.setIsActive(false);
        userService.save(user);
        mailService.sendEmail(user.getEmail(), "Kafein POC", "http://213.159.2.75/auth/confirm?token=" + token);
    }

    public String confirmUser(String token) {
        if (jwtTokenUtil.isTokenExpired(token)) {
            throw new BadRequestException("Token is expired");
        }
        var claims = jwtTokenUtil.getAllClaimsFromToken(token);
        var email =claims.get("sub", String.class);
        var user = userService.getByEmail(email);
        user.setIsActive(true);
        userService.save(userMapper.toEntity(user));
        return jwtTokenUtil.doGenerateToken(new HashMap<>(), user.getUsername());
    }
}
