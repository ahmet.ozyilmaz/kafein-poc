package org.kafein.poc.userservice.service;

import io.jsonwebtoken.Claims;
import lombok.AllArgsConstructor;
import org.kafein.poc.userservice.dto.ResetPasswordDto;
import org.kafein.poc.userservice.dto.UserDto;
import org.kafein.poc.userservice.entity.User;
import org.kafein.poc.userservice.mapper.UserMapper;
import org.kafein.poc.userservice.repository.BaseRepository;
import org.kafein.poc.userservice.repository.UserRepository;
import org.springframework.context.annotation.Bean;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.Optional;

import static org.kafein.poc.userservice.service.JwtUserDetailsService.passwordEncoder;

@Service
@AllArgsConstructor
public class UserService extends AbstractService<User> {

    private final UserRepository userRepository;
    
    private final UserMapper userMapper;

    @Override
    protected void afterSave(User entity) {
        super.afterSave(entity);

    }
/*
    @PostConstruct
    private void init() {
        User user = new User();
        user.setUsername("test.user1@kafein.com");
        user.setPassword(passwordEncoder.encode("12345678"));
        user.setBirthDate(new Date());
        user.setEmail("test.user1@kafein.com");
        user.setPhone("1111111111");
        //userRepository.save(user);
    }


 */


    public UserDto getByUsername(String username) {
        Optional<User> userOpt = userRepository.getByUsername(username);
        if(userOpt.isEmpty()) {
            throw new RuntimeException("Not found");
        }
        return this.userMapper.toDto(userOpt.get());
    }
    
    @Override
    public BaseRepository<User> getRepository() {
        return userRepository;
    }

    public void resetPassword(ResetPasswordDto resetPasswordDto) {
        var user = getCurrentUser();
        if (user == null) {
            throw new RuntimeException("Not found");
        }
        if (!passwordEncoder.matches(resetPasswordDto.getOldPassword(), user.getPassword())) {
            throw new RuntimeException("Password is wrong");
        }
        user.setPassword(passwordEncoder.encode(resetPasswordDto.getNewPassword()));
        userRepository.save(user);
    }

    public User getCurrentUser() {
        var authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication.getPrincipal().equals("anonymousUser")) {
            return null;
        }
        var email = authentication.getPrincipal().toString();
        return userRepository.getByEmail(email).orElse(null);
    }

    public UserDto getByEmail(String email) {
        Optional<User> userOpt = userRepository.getByEmail(email);
        if(userOpt.isEmpty()) {
            throw new RuntimeException("Not found");
        }
        return this.userMapper.toDto(userOpt.get());
    }
}
