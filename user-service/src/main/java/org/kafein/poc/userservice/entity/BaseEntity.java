package org.kafein.poc.userservice.entity;


import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@MappedSuperclass
public class BaseEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "creation_date")
	private Date creationDate;
	
	@JoinColumn(name = "created_by", referencedColumnName = "id")
	private Long createdBy;
	
	@Column(name = "last_update_date")
	private Date lastUpdateDate;
	
	@JoinColumn(name = "edited_by", referencedColumnName = "id")
	private Long editedBy;
	
	@Column(name = "is_active")
	private Boolean isActive;
	
	@Column(name = "is_deleted")
	private Boolean isDeleted;
}

