package org.kafein.poc.userservice.dto;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
public class BaseEntityDto implements Serializable {
	
	private Long id;
	
	private Date creationDate;
	
	private Long createdBy;
	
	private Date lastUpdateDate;
	
	private Long editedBy;
	
	private Boolean isActive;
	
	private Boolean isDeleted;
	
}
