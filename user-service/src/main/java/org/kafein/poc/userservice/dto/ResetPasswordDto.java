package org.kafein.poc.userservice.dto;

import lombok.Data;

@Data
public class ResetPasswordDto {

    private String oldPassword;

    private String newPassword;
}
