package org.kafein.poc.userservice.dto;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JwtRequest {
    
    @NotNull
    @NotEmpty
    private String username;
    
    @NotNull
    @Size(min = 8, max = 20,message = "Field must be at least 8 characters long")
    private String password;
}