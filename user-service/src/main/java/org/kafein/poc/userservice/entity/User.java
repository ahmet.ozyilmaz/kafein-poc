package org.kafein.poc.userservice.entity;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name = "kafein_user")
public class User extends BaseEntity {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String username;
    
    private String name;
    
    @Column
    private String password;
    
    @Column
    private Date birthDate;
    
    @Column(unique = true)
    private String email;
    
    @Column
    private String phone;
    
    @Column
    private Long customerId;

    @Column
    private Boolean active = false;

    @Column
    private String activationCode;
}
