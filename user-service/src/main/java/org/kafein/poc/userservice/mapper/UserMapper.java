package org.kafein.poc.userservice.mapper;

import org.kafein.poc.userservice.dto.UserDto;
import org.kafein.poc.userservice.entity.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper extends AbstractMapper<User, UserDto> {
}
