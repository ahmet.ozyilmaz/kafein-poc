package org.kafein.poc.userservice.exception;

import org.springframework.http.HttpStatus;

public class MediaExistsException extends AbstractException {
    public MediaExistsException(String message) {
        super(HttpStatus.BAD_REQUEST,"418" , message);
    }
}

