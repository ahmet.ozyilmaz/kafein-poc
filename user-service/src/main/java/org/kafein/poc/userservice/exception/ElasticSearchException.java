package org.kafein.poc.userservice.exception;


import org.springframework.http.HttpStatus;

public class ElasticSearchException extends AbstractException {

    public ElasticSearchException(String message) {
        super(HttpStatus.BAD_REQUEST,message,message);
    }
}
