package org.kafein.poc.userservice.dto;

import com.fasterxml.jackson.annotation.JsonValue;

public enum MessageType {
	VALIDATION_ERROR, API_ERROR, ERROR_PARAM;
	
	@JsonValue
	public int toValue() {
		return ordinal();
	}
}
