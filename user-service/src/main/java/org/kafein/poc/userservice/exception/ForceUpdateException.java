package org.kafein.poc.userservice.exception;

import org.springframework.http.HttpStatus;

public class ForceUpdateException extends AbstractException{

    public ForceUpdateException(String message) {
        super(HttpStatus.UPGRADE_REQUIRED,"426" , message);
    }
}
