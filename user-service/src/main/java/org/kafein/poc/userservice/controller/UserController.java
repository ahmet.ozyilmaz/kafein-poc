package org.kafein.poc.userservice.controller;

import lombok.RequiredArgsConstructor;
import org.kafein.poc.userservice.dto.ResetPasswordDto;
import org.kafein.poc.userservice.mapper.AbstractMapper;
import org.kafein.poc.userservice.mapper.UserMapper;
import org.kafein.poc.userservice.dto.UserDto;
import org.kafein.poc.userservice.entity.User;
import org.kafein.poc.userservice.service.AbstractService;
import org.kafein.poc.userservice.service.UserService;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@RequiredArgsConstructor
public class UserController extends AbstractController<User, UserDto> {
    
    private final UserService userService;
    
    private final UserMapper userMapper;
    
    @GetMapping("/username/{username}")
    public UserDto getByUsername(@PathVariable String username) {
        return userService.getByUsername(username);
    }

    @PostMapping("/reset-password")
    public void resetPassword(@RequestBody ResetPasswordDto resetPasswordDto) {
        userService.resetPassword(resetPasswordDto);
    }
    
    @Override
    public AbstractService<User> getService() {
        return userService;
    }
    
    @Override
    public AbstractMapper<User, UserDto> getMapper() {
        return userMapper;
    }
}
