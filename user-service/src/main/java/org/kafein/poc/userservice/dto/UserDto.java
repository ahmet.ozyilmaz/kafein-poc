package org.kafein.poc.userservice.dto;

import lombok.Data;
import org.kafein.poc.userservice.entity.BaseEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class UserDto extends BaseEntityDto {
    
    
    @NotNull
    private String username;
    
    @NotNull
    private String password;
    
    private Date birthDate;
    
    @NotNull
    private String email;
    
    private String phone;
    
    private String name;
    
    private Long customerId;
    
}
