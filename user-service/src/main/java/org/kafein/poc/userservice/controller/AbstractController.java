package org.kafein.poc.userservice.controller;


import jdk.jshell.spi.ExecutionControl;
import org.kafein.poc.userservice.dto.BaseEntityDto;
import org.kafein.poc.userservice.dto.PageableConstants;
import org.kafein.poc.userservice.entity.BaseEntity;
import org.kafein.poc.userservice.mapper.AbstractMapper;
import org.kafein.poc.userservice.service.AbstractService;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

public abstract class AbstractController<ENTITY extends BaseEntity, DTO extends BaseEntityDto> {
	
	@GetMapping("/{id}")
	public ResponseEntity<DTO> get(@PathVariable Long id) {
		return ResponseEntity.ok(getMapper().toDto(getService().get(id)));
	}
	
	@PostMapping
	public ResponseEntity<DTO> save(@Valid @RequestBody DTO dto) {
		return ResponseEntity.ok(getMapper().toDto(getService().save(getMapper().toEntity(dto))));
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<DTO> update(@PathVariable Long id, @Valid @RequestBody DTO dto) {
		return ResponseEntity.ok(getMapper().toDto(getService().put(id, getMapper().toEntity(dto))));
	}
	
	@GetMapping("/all")
	public ResponseEntity<PageImpl<DTO>> getAll(
			@RequestParam(defaultValue = PageableConstants.PAGE_NO_DEFAULT_VALUE) Integer pageNo,
			@RequestParam(defaultValue = PageableConstants.PAGE_SIZE_DEFAULT_VALUE) Integer pageSize
	)
			throws ExecutionControl.NotImplementedException {
		Pageable pageable = PageRequest.of(pageNo, pageSize);
		var page = getService().allByPage(pageable);
		
		var pageData = new PageImpl<>(getMapper().toDto(page.toList()), pageable, page.getTotalElements());
		return ResponseEntity.ok(pageData);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable Long id) {
		getService().delete(id);
		return ResponseEntity.ok().build();
	}
	
	public abstract AbstractService<ENTITY> getService();
	
	public abstract AbstractMapper<ENTITY, DTO> getMapper();
}
