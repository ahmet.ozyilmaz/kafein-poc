package org.kafein.poc.userservice.service;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.kafein.poc.userservice.dto.JwtRequest;
import org.kafein.poc.userservice.dto.UserDto;
import org.kafein.poc.userservice.entity.User;
import org.kafein.poc.userservice.mapper.UserMapper;
import org.kafein.poc.userservice.service.mail.MailService;
import org.kafein.poc.userservice.util.PasswordUtil;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

public class JwtUserDetailsServiceTest {

    @Mock
    private UserService userService;

    @Mock
    private MailService mailService;

    @Mock
    private UserMapper userMapper;

    @Mock
    private JwtTokenUtil jwtTokenUtil;

    @InjectMocks
    private JwtUserDetailsService jwtUserDetailsService;

    private final PasswordEncoder passwordEncoder = new BCryptPasswordEncoder();

    @BeforeEach
    public void setUp() {
        userService = mock(UserService.class);
        mailService = mock(MailService.class);
        userMapper = mock(UserMapper.class);
        jwtTokenUtil = mock(JwtTokenUtil.class);
        jwtUserDetailsService = new JwtUserDetailsService(userService, mailService, userMapper, jwtTokenUtil);
    }

    @Test
    public void generateToken_WithValidCredentials_ReturnsToken() {
        // Arrange
        JwtRequest request = new JwtRequest("username", "password");
        UserDto userDto = new UserDto();
        userDto.setUsername("username");
        userDto.setPassword(passwordEncoder.encode("password"));
        when(userService.getByUsername(request.getUsername())).thenReturn(userDto);
        when(jwtTokenUtil.doGenerateToken(anyMap(), eq(request.getUsername()))).thenReturn("token");

        // Act
        String token = jwtUserDetailsService.generateToken(request);

        // Assert
        assertNotNull(token);
        assertEquals("token", token);
    }

    @Test
    public void generateToken_WithInvalidCredentials_ThrowsException() {
        // Arrange
        JwtRequest request = new JwtRequest("username", "password");
        UserDto userDto = new UserDto();
        userDto.setUsername("username");
        userDto.setPassword(passwordEncoder.encode("wrongPassword"));
        when(userService.getByUsername(request.getUsername())).thenReturn(userDto);

        // Assert
        assertThrows(RuntimeException.class, () -> {
            // Act
            jwtUserDetailsService.generateToken(request);
        });
    }


    @Test
    public void sendMailToForgotPassword_WithInvalidEmail_ThrowsException() {
        // Arrange
        String email = "test@example.com";
        when(userService.getByEmail(email)).thenReturn(null);

        // Assert
        assertThrows(RuntimeException.class, () -> {
            // Act
            jwtUserDetailsService.sendMailToForgotPassword(email);
        });
    }

    @Test
    public void activateUser_WithInvalidToken_ThrowsException() {
        // Arrange
        String token = "token";
        when(jwtTokenUtil.getUsernameFromToken(token)).thenReturn(null);


    }

}