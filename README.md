# Add new spring project

nx g @nxrocks/nx-spring-boot:new

# run the spring boot app

nx serve [spring-boot-app-name]


# User Service

init username = test.user1@kafein.com
init user password = 12345678

Swagger UI: http://localhost:9001/swagger-ui/index.html

# Deployment BE:

ssh root@213.159.2.75 -p 23422
89XkUGdEZbtspFPY
scp -P 23422 oto-expert-0.0.1.jar root@213.159.2.75:/app/prod

scp -P 23422 user-service/target/user-service-0.0.1-SNAPSHOT.jar root@213.159.2.75:/app/kafein-poc

scp -P 23422 service-registry/target/registry-server-0.0.1-SNAPSHOT.jar root@213.159.2.75:/app/kafein-poc

scp -P 23422 business-service/target/business-service-0.0.1-SNAPSHOT.jar root@213.159.2.75:/app/kafein-poc

scp -P 23422 gateway/target/gateway-0.0.1-SNAPSHOT.jar root@213.159.2.75:/app/kafein-poc

scp -P 23422 backend/target/demo-0.0.1-SNAPSHOT.jar root@213.159.2.75:/app/kafein-poc 
