package org.kafein.poc.businessservice.mapper;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.kafein.poc.businessservice.dto.OrderItemDto;
import org.kafein.poc.businessservice.entity.OrderItem;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-04-14T05:33:33+0300",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 17.0.2 (Oracle Corporation)"
)
@Component
public class OrderItemMapperImpl implements OrderItemMapper {

    @Override
    public OrderItem toEntity(OrderItemDto dto) {
        if ( dto == null ) {
            return null;
        }

        OrderItem orderItem = new OrderItem();

        orderItem.setId( dto.getId() );
        orderItem.setCreationDate( dto.getCreationDate() );
        orderItem.setLastUpdateDate( dto.getLastUpdateDate() );
        orderItem.setIsActive( dto.getIsActive() );
        orderItem.setIsDeleted( dto.getIsDeleted() );

        return orderItem;
    }

    @Override
    public OrderItemDto toDto(OrderItem entity) {
        if ( entity == null ) {
            return null;
        }

        OrderItemDto orderItemDto = new OrderItemDto();

        orderItemDto.setId( entity.getId() );
        orderItemDto.setCreationDate( entity.getCreationDate() );
        orderItemDto.setLastUpdateDate( entity.getLastUpdateDate() );
        orderItemDto.setIsActive( entity.getIsActive() );
        orderItemDto.setIsDeleted( entity.getIsDeleted() );

        return orderItemDto;
    }

    @Override
    public List<OrderItem> toEntity(List<OrderItemDto> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<OrderItem> list = new ArrayList<OrderItem>( dtoList.size() );
        for ( OrderItemDto orderItemDto : dtoList ) {
            list.add( toEntity( orderItemDto ) );
        }

        return list;
    }

    @Override
    public List<OrderItemDto> toDto(List<OrderItem> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<OrderItemDto> list = new ArrayList<OrderItemDto>( entityList.size() );
        for ( OrderItem orderItem : entityList ) {
            list.add( toDto( orderItem ) );
        }

        return list;
    }
}
