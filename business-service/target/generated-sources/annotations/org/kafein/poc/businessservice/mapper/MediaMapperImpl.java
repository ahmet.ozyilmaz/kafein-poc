package org.kafein.poc.businessservice.mapper;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.kafein.poc.businessservice.entity.Media;
import org.kafein.poc.businessservice.entity.MediaDto;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-04-14T05:33:34+0300",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 17.0.2 (Oracle Corporation)"
)
@Component
public class MediaMapperImpl implements MediaMapper {

    @Override
    public Media toEntity(MediaDto dto) {
        if ( dto == null ) {
            return null;
        }

        Media media = new Media();

        media.setId( dto.getId() );
        media.setCreationDate( dto.getCreationDate() );
        media.setLastUpdateDate( dto.getLastUpdateDate() );
        media.setIsActive( dto.getIsActive() );
        media.setIsDeleted( dto.getIsDeleted() );
        media.setProductId( dto.getProductId() );
        media.setObjectId( dto.getObjectId() );
        media.setThumbnail( dto.getThumbnail() );
        media.setDescription( dto.getDescription() );
        media.setFileName( dto.getFileName() );
        media.setSize( dto.getSize() );

        return media;
    }

    @Override
    public MediaDto toDto(Media entity) {
        if ( entity == null ) {
            return null;
        }

        MediaDto mediaDto = new MediaDto();

        mediaDto.setId( entity.getId() );
        mediaDto.setCreationDate( entity.getCreationDate() );
        mediaDto.setLastUpdateDate( entity.getLastUpdateDate() );
        mediaDto.setIsActive( entity.getIsActive() );
        mediaDto.setIsDeleted( entity.getIsDeleted() );
        if ( entity.getProductId() != null ) {
            mediaDto.setProductId( entity.getProductId() );
        }
        mediaDto.setObjectId( entity.getObjectId() );
        mediaDto.setThumbnail( entity.getThumbnail() );
        mediaDto.setDescription( entity.getDescription() );
        mediaDto.setFileName( entity.getFileName() );
        mediaDto.setSize( entity.getSize() );

        return mediaDto;
    }

    @Override
    public List<Media> toEntity(List<MediaDto> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Media> list = new ArrayList<Media>( dtoList.size() );
        for ( MediaDto mediaDto : dtoList ) {
            list.add( toEntity( mediaDto ) );
        }

        return list;
    }

    @Override
    public List<MediaDto> toDto(List<Media> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<MediaDto> list = new ArrayList<MediaDto>( entityList.size() );
        for ( Media media : entityList ) {
            list.add( toDto( media ) );
        }

        return list;
    }
}
