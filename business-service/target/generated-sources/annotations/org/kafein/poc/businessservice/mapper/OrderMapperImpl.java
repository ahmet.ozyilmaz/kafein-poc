package org.kafein.poc.businessservice.mapper;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.kafein.poc.businessservice.dto.OrderDto;
import org.kafein.poc.businessservice.entity.Order;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-04-14T05:33:34+0300",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 17.0.2 (Oracle Corporation)"
)
@Component
public class OrderMapperImpl implements OrderMapper {

    @Override
    public Order toEntity(OrderDto dto) {
        if ( dto == null ) {
            return null;
        }

        Order order = new Order();

        order.setId( dto.getId() );
        order.setCreationDate( dto.getCreationDate() );
        order.setLastUpdateDate( dto.getLastUpdateDate() );
        order.setIsActive( dto.getIsActive() );
        order.setIsDeleted( dto.getIsDeleted() );

        return order;
    }

    @Override
    public OrderDto toDto(Order entity) {
        if ( entity == null ) {
            return null;
        }

        OrderDto orderDto = new OrderDto();

        orderDto.setId( entity.getId() );
        orderDto.setCreationDate( entity.getCreationDate() );
        orderDto.setLastUpdateDate( entity.getLastUpdateDate() );
        orderDto.setIsActive( entity.getIsActive() );
        orderDto.setIsDeleted( entity.getIsDeleted() );

        return orderDto;
    }

    @Override
    public List<Order> toEntity(List<OrderDto> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Order> list = new ArrayList<Order>( dtoList.size() );
        for ( OrderDto orderDto : dtoList ) {
            list.add( toEntity( orderDto ) );
        }

        return list;
    }

    @Override
    public List<OrderDto> toDto(List<Order> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<OrderDto> list = new ArrayList<OrderDto>( entityList.size() );
        for ( Order order : entityList ) {
            list.add( toDto( order ) );
        }

        return list;
    }
}
