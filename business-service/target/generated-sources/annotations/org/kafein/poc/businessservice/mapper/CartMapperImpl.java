package org.kafein.poc.businessservice.mapper;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.kafein.poc.businessservice.dto.CartDto;
import org.kafein.poc.businessservice.entity.Cart;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-04-14T05:33:34+0300",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 17.0.2 (Oracle Corporation)"
)
@Component
public class CartMapperImpl implements CartMapper {

    @Override
    public Cart toEntity(CartDto dto) {
        if ( dto == null ) {
            return null;
        }

        Cart cart = new Cart();

        cart.setId( dto.getId() );
        cart.setCreationDate( dto.getCreationDate() );
        cart.setLastUpdateDate( dto.getLastUpdateDate() );
        cart.setIsActive( dto.getIsActive() );
        cart.setIsDeleted( dto.getIsDeleted() );

        return cart;
    }

    @Override
    public CartDto toDto(Cart entity) {
        if ( entity == null ) {
            return null;
        }

        CartDto cartDto = new CartDto();

        cartDto.setId( entity.getId() );
        cartDto.setCreationDate( entity.getCreationDate() );
        cartDto.setLastUpdateDate( entity.getLastUpdateDate() );
        cartDto.setIsActive( entity.getIsActive() );
        cartDto.setIsDeleted( entity.getIsDeleted() );

        return cartDto;
    }

    @Override
    public List<Cart> toEntity(List<CartDto> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Cart> list = new ArrayList<Cart>( dtoList.size() );
        for ( CartDto cartDto : dtoList ) {
            list.add( toEntity( cartDto ) );
        }

        return list;
    }

    @Override
    public List<CartDto> toDto(List<Cart> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<CartDto> list = new ArrayList<CartDto>( entityList.size() );
        for ( Cart cart : entityList ) {
            list.add( toDto( cart ) );
        }

        return list;
    }
}
