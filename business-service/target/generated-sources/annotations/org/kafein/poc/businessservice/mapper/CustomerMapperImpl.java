package org.kafein.poc.businessservice.mapper;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.kafein.poc.businessservice.dto.CustomerDto;
import org.kafein.poc.businessservice.entity.Customer;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-04-14T05:33:34+0300",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 17.0.2 (Oracle Corporation)"
)
@Component
public class CustomerMapperImpl implements CustomerMapper {

    @Override
    public Customer toEntity(CustomerDto dto) {
        if ( dto == null ) {
            return null;
        }

        Customer customer = new Customer();

        customer.setId( dto.getId() );
        customer.setCreationDate( dto.getCreationDate() );
        customer.setLastUpdateDate( dto.getLastUpdateDate() );
        customer.setIsActive( dto.getIsActive() );
        customer.setIsDeleted( dto.getIsDeleted() );

        return customer;
    }

    @Override
    public CustomerDto toDto(Customer entity) {
        if ( entity == null ) {
            return null;
        }

        CustomerDto customerDto = new CustomerDto();

        customerDto.setId( entity.getId() );
        customerDto.setCreationDate( entity.getCreationDate() );
        customerDto.setLastUpdateDate( entity.getLastUpdateDate() );
        customerDto.setIsActive( entity.getIsActive() );
        customerDto.setIsDeleted( entity.getIsDeleted() );

        return customerDto;
    }

    @Override
    public List<Customer> toEntity(List<CustomerDto> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Customer> list = new ArrayList<Customer>( dtoList.size() );
        for ( CustomerDto customerDto : dtoList ) {
            list.add( toEntity( customerDto ) );
        }

        return list;
    }

    @Override
    public List<CustomerDto> toDto(List<Customer> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<CustomerDto> list = new ArrayList<CustomerDto>( entityList.size() );
        for ( Customer customer : entityList ) {
            list.add( toDto( customer ) );
        }

        return list;
    }
}
