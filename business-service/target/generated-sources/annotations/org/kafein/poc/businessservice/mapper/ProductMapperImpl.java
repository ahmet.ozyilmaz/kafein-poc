package org.kafein.poc.businessservice.mapper;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.annotation.processing.Generated;
import org.kafein.poc.businessservice.dto.ProductDto;
import org.kafein.poc.businessservice.entity.Media;
import org.kafein.poc.businessservice.entity.MediaDto;
import org.kafein.poc.businessservice.entity.Product;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-04-14T05:33:34+0300",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 17.0.2 (Oracle Corporation)"
)
@Component
public class ProductMapperImpl implements ProductMapper {

    @Override
    public Product toEntity(ProductDto dto) {
        if ( dto == null ) {
            return null;
        }

        Product product = new Product();

        product.setId( dto.getId() );
        product.setCreationDate( dto.getCreationDate() );
        product.setLastUpdateDate( dto.getLastUpdateDate() );
        product.setIsActive( dto.getIsActive() );
        product.setIsDeleted( dto.getIsDeleted() );
        product.setName( dto.getName() );
        product.setDescription( dto.getDescription() );
        product.setPrice( dto.getPrice() );
        product.setStock( dto.getStock() );
        product.setMediaSet( mediaDtoSetToMediaSet( dto.getMediaSet() ) );

        return product;
    }

    @Override
    public ProductDto toDto(Product entity) {
        if ( entity == null ) {
            return null;
        }

        ProductDto productDto = new ProductDto();

        productDto.setId( entity.getId() );
        productDto.setCreationDate( entity.getCreationDate() );
        productDto.setLastUpdateDate( entity.getLastUpdateDate() );
        productDto.setIsActive( entity.getIsActive() );
        productDto.setIsDeleted( entity.getIsDeleted() );
        productDto.setName( entity.getName() );
        productDto.setDescription( entity.getDescription() );
        productDto.setPrice( entity.getPrice() );
        productDto.setStock( entity.getStock() );
        productDto.setMediaSet( mediaSetToMediaDtoSet( entity.getMediaSet() ) );

        return productDto;
    }

    @Override
    public List<Product> toEntity(List<ProductDto> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<Product> list = new ArrayList<Product>( dtoList.size() );
        for ( ProductDto productDto : dtoList ) {
            list.add( toEntity( productDto ) );
        }

        return list;
    }

    @Override
    public List<ProductDto> toDto(List<Product> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<ProductDto> list = new ArrayList<ProductDto>( entityList.size() );
        for ( Product product : entityList ) {
            list.add( toDto( product ) );
        }

        return list;
    }

    protected Media mediaDtoToMedia(MediaDto mediaDto) {
        if ( mediaDto == null ) {
            return null;
        }

        Media media = new Media();

        media.setId( mediaDto.getId() );
        media.setCreationDate( mediaDto.getCreationDate() );
        media.setLastUpdateDate( mediaDto.getLastUpdateDate() );
        media.setIsActive( mediaDto.getIsActive() );
        media.setIsDeleted( mediaDto.getIsDeleted() );
        media.setProductId( mediaDto.getProductId() );
        media.setObjectId( mediaDto.getObjectId() );
        media.setThumbnail( mediaDto.getThumbnail() );
        media.setDescription( mediaDto.getDescription() );
        media.setFileName( mediaDto.getFileName() );
        media.setSize( mediaDto.getSize() );

        return media;
    }

    protected Set<Media> mediaDtoSetToMediaSet(Set<MediaDto> set) {
        if ( set == null ) {
            return null;
        }

        Set<Media> set1 = new LinkedHashSet<Media>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( MediaDto mediaDto : set ) {
            set1.add( mediaDtoToMedia( mediaDto ) );
        }

        return set1;
    }

    protected MediaDto mediaToMediaDto(Media media) {
        if ( media == null ) {
            return null;
        }

        MediaDto mediaDto = new MediaDto();

        mediaDto.setId( media.getId() );
        mediaDto.setCreationDate( media.getCreationDate() );
        mediaDto.setLastUpdateDate( media.getLastUpdateDate() );
        mediaDto.setIsActive( media.getIsActive() );
        mediaDto.setIsDeleted( media.getIsDeleted() );
        if ( media.getProductId() != null ) {
            mediaDto.setProductId( media.getProductId() );
        }
        mediaDto.setObjectId( media.getObjectId() );
        mediaDto.setThumbnail( media.getThumbnail() );
        mediaDto.setDescription( media.getDescription() );
        mediaDto.setFileName( media.getFileName() );
        mediaDto.setSize( media.getSize() );

        return mediaDto;
    }

    protected Set<MediaDto> mediaSetToMediaDtoSet(Set<Media> set) {
        if ( set == null ) {
            return null;
        }

        Set<MediaDto> set1 = new LinkedHashSet<MediaDto>( Math.max( (int) ( set.size() / .75f ) + 1, 16 ) );
        for ( Media media : set ) {
            set1.add( mediaToMediaDto( media ) );
        }

        return set1;
    }
}
