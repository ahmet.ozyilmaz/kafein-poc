package org.kafein.poc.businessservice.mapper;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.processing.Generated;
import org.kafein.poc.businessservice.dto.CartItemDto;
import org.kafein.poc.businessservice.entity.CartItem;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2023-04-14T05:33:34+0300",
    comments = "version: 1.5.3.Final, compiler: javac, environment: Java 17.0.2 (Oracle Corporation)"
)
@Component
public class CartItemMapperImpl implements CartItemMapper {

    @Override
    public CartItem toEntity(CartItemDto dto) {
        if ( dto == null ) {
            return null;
        }

        CartItem cartItem = new CartItem();

        cartItem.setId( dto.getId() );
        cartItem.setCreationDate( dto.getCreationDate() );
        cartItem.setLastUpdateDate( dto.getLastUpdateDate() );
        cartItem.setIsActive( dto.getIsActive() );
        cartItem.setIsDeleted( dto.getIsDeleted() );

        return cartItem;
    }

    @Override
    public CartItemDto toDto(CartItem entity) {
        if ( entity == null ) {
            return null;
        }

        CartItemDto cartItemDto = new CartItemDto();

        cartItemDto.setId( entity.getId() );
        cartItemDto.setCreationDate( entity.getCreationDate() );
        cartItemDto.setLastUpdateDate( entity.getLastUpdateDate() );
        cartItemDto.setIsActive( entity.getIsActive() );
        cartItemDto.setIsDeleted( entity.getIsDeleted() );

        return cartItemDto;
    }

    @Override
    public List<CartItem> toEntity(List<CartItemDto> dtoList) {
        if ( dtoList == null ) {
            return null;
        }

        List<CartItem> list = new ArrayList<CartItem>( dtoList.size() );
        for ( CartItemDto cartItemDto : dtoList ) {
            list.add( toEntity( cartItemDto ) );
        }

        return list;
    }

    @Override
    public List<CartItemDto> toDto(List<CartItem> entityList) {
        if ( entityList == null ) {
            return null;
        }

        List<CartItemDto> list = new ArrayList<CartItemDto>( entityList.size() );
        for ( CartItem cartItem : entityList ) {
            list.add( toDto( cartItem ) );
        }

        return list;
    }
}
