package org.kafein.poc.businessservice.mapper;

import org.kafein.poc.businessservice.dto.CustomerDto;
import org.kafein.poc.businessservice.entity.Customer;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CustomerMapper extends AbstractMapper<Customer, CustomerDto> {
}
