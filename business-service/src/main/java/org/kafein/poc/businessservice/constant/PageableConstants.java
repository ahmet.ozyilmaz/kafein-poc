package org.kafein.poc.businessservice.constant;

public class PageableConstants {
	public static final String PAGE_NO_DEFAULT_VALUE = "0";
	
	public static final String PAGE_SIZE_DEFAULT_VALUE = "10";
	
	public static final String PAGE_SIZE_MAX_VALUE = "99999";
	
	public static final String PAGE_SORT_DEFAULT_VALUE = "DESC";
	
	public static final String PROPERTY_DEFAULT_VALUE = "id";
}
