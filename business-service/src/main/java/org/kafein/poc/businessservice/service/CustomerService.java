package org.kafein.poc.businessservice.service;

import lombok.RequiredArgsConstructor;
import org.kafein.poc.businessservice.entity.Customer;
import org.kafein.poc.businessservice.mapper.CustomerMapper;
import org.kafein.poc.businessservice.repository.BaseRepository;
import org.kafein.poc.businessservice.repository.CustomerRepository;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class CustomerService extends AbstractService<Customer> {
	
	private final CustomerRepository customerRepository;
	
	private final CustomerMapper customerMapper;
	
	@Override
	public BaseRepository<Customer> getRepository() {
		return customerRepository;
	}
}
