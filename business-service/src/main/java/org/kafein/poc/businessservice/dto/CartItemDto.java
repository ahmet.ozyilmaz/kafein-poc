package org.kafein.poc.businessservice.dto;

import lombok.Data;

@Data
public class CartItemDto extends BaseEntityDto {
	
	
	private Long cartId;
	
	private ProductDto product;
	
	private Integer quantity;
}