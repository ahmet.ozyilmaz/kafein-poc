package org.kafein.poc.businessservice.dto;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public enum ImageOrientationType {
	HORIZONTAL_NORMAL(1),
	MIRROR_HORIZONTAL(2),
	ROTATE_180(3),
	MIRROR_VERTICAL(4),
	MIRROR_HORIZONTAL_ROTATE_270_CW(5),
	ROTATE_90_CW(6),
	MIRROR_HORIZONTAL_ROTATE_90_CW(7),
	ROTATE_270_CW(8);
	
	private final int orientation;
	
	private static final Map<Integer, ImageOrientationType> map;
	
	static {
		map = new HashMap<>();
		for (ImageOrientationType v : ImageOrientationType.values()) {
			map.put(v.orientation, v);
		}
	}
	
	public static ImageOrientationType valueOf(int orientation) {
		return map.get(orientation);
	}
	
	ImageOrientationType(int orientation) {
		this.orientation = orientation;
	}
	
	public static Map<String, Integer> getAll() {
		return Arrays.stream(values()).collect(Collectors.toMap(Enum::name, e -> e.orientation));
	}
}