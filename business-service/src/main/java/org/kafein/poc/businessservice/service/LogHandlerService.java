package org.kafein.poc.businessservice.service;

import org.kafein.poc.businessservice.entity.LogMessage;
import org.kafein.poc.businessservice.repository.LogMessageRepository;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class LogHandlerService {
	
	private final LogMessageRepository logMessageRepository;
	
	public LogHandlerService(LogMessageRepository logMessageRepository) {
		this.logMessageRepository = logMessageRepository;
	}
	
	@KafkaListener(topics = "business-logs")
	public void consumeLogMessage(String message) {
		logMessageRepository.save(new LogMessage(message));
	}
}
