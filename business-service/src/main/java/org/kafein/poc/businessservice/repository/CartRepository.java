package org.kafein.poc.businessservice.repository;

import org.kafein.poc.businessservice.entity.Cart;
import org.springframework.stereotype.Repository;

@Repository
public interface CartRepository extends BaseRepository<Cart> {
}
