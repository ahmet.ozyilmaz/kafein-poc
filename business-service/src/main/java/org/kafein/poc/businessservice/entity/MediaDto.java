package org.kafein.poc.businessservice.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.kafein.poc.businessservice.dto.BaseEntityDto;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MediaDto extends BaseEntityDto {
	
	private long productId;
	
	private String objectId;
	
	private String thumbnail;
	
	private String description;
	
	private Boolean useReport;
	
	private String fileName;
	
	private Double size;
	
}
