package org.kafein.poc.businessservice.mapper;

import org.kafein.poc.businessservice.dto.CartItemDto;
import org.kafein.poc.businessservice.entity.CartItem;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CartItemMapper extends AbstractMapper<CartItem, CartItemDto> {
}
