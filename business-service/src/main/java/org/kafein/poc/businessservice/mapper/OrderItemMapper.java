package org.kafein.poc.businessservice.mapper;

import org.kafein.poc.businessservice.dto.OrderItemDto;
import org.kafein.poc.businessservice.entity.OrderItem;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OrderItemMapper extends AbstractMapper<OrderItem, OrderItemDto> {
}
