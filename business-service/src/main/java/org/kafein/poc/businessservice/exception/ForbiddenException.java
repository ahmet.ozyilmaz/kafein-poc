package org.kafein.poc.businessservice.exception;

import org.springframework.http.HttpStatus;

public class ForbiddenException extends AbstractException {
	public ForbiddenException(String message) {
		super(message);
	}
	
	@Override
	public HttpStatus getStatus() {
		return HttpStatus.FORBIDDEN;
	}
}
