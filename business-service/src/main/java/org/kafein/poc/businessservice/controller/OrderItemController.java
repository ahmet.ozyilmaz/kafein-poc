package org.kafein.poc.businessservice.controller;

import lombok.RequiredArgsConstructor;
import org.kafein.poc.businessservice.dto.OrderItemDto;
import org.kafein.poc.businessservice.entity.OrderItem;
import org.kafein.poc.businessservice.mapper.AbstractMapper;
import org.kafein.poc.businessservice.mapper.OrderItemMapper;
import org.kafein.poc.businessservice.service.AbstractService;
import org.kafein.poc.businessservice.service.OrderItemService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order-item")
@RequiredArgsConstructor
public class OrderItemController extends AbstractController<OrderItem, OrderItemDto> {
	
	private final OrderItemService orderItemService;
	
	private final OrderItemMapper orderItemMapper;
	
	@Override
	public AbstractService<OrderItem> getService() {
		return orderItemService;
	}
	
	@Override
	public AbstractMapper<OrderItem, OrderItemDto> getMapper() {
		return orderItemMapper;
	}
}
