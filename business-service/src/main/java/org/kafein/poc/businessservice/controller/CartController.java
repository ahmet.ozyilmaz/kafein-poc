package org.kafein.poc.businessservice.controller;

import lombok.RequiredArgsConstructor;
import org.kafein.poc.businessservice.dto.CartDto;
import org.kafein.poc.businessservice.entity.Cart;
import org.kafein.poc.businessservice.mapper.AbstractMapper;
import org.kafein.poc.businessservice.mapper.CartMapper;
import org.kafein.poc.businessservice.service.AbstractService;
import org.kafein.poc.businessservice.service.CartService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cart")
@RequiredArgsConstructor
public class CartController extends AbstractController<Cart, CartDto> {
	
	private final CartService cartService;
	
	private final CartMapper cartMapper;
	
	@Override
	public AbstractService<Cart> getService() {
		return cartService;
	}
	
	@Override
	public AbstractMapper<Cart, CartDto> getMapper() {
		return cartMapper;
	}
}
