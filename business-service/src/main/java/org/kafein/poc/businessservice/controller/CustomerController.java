package org.kafein.poc.businessservice.controller;

import lombok.RequiredArgsConstructor;
import org.kafein.poc.businessservice.dto.CustomerDto;
import org.kafein.poc.businessservice.entity.Customer;
import org.kafein.poc.businessservice.mapper.AbstractMapper;
import org.kafein.poc.businessservice.mapper.CustomerMapper;
import org.kafein.poc.businessservice.service.AbstractService;
import org.kafein.poc.businessservice.service.CustomerService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customer")
@RequiredArgsConstructor
public class CustomerController extends AbstractController<Customer, CustomerDto> {
	
	private final CustomerService customerService;
	
	private final CustomerMapper customerMapper;
	
	@Override
	public AbstractService<Customer> getService() {
		return customerService;
	}
	
	@Override
	public AbstractMapper<Customer, CustomerDto> getMapper() {
		return customerMapper;
	}
}
