package org.kafein.poc.businessservice.entity;


import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.util.Date;

@Data
@MappedSuperclass
public class BaseEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "creation_date")
	@CreatedDate
	private Date creationDate;
	
	@Column(name = "last_update_date")
	@LastModifiedDate
	private Date lastUpdateDate;
	
	@Column(name = "is_active")
	private Boolean isActive = true;
	
	@Column(name = "is_deleted")
	private Boolean isDeleted = false;
	
	@PrePersist
	public void onCreate() {
		this.creationDate = new Date();
		this.lastUpdateDate = new Date();
	}
	
	@PreUpdate
	public void onUpdate() {
		this.lastUpdateDate = new Date();
	}
}

