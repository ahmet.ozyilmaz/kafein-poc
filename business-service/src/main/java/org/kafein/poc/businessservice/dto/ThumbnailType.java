package org.kafein.poc.businessservice.dto;

public enum ThumbnailType {
	WEB_MAIN(300, 300),
	WEB(150, 70),
	MONITORING(128, 128);
	
	
	public int width;
	public int height;
	
	ThumbnailType(int width, int height) {
		this.width = width;
		this.height = height;
	}
}
