package org.kafein.poc.businessservice.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "product")
public class Product extends BaseEntity {
	
	private String name;
	private String description;
	private Double price;
	private Integer stock;
	
	
	@OneToMany(mappedBy = "product", fetch = FetchType.EAGER)
	private Set<Media> mediaSet;
	
}