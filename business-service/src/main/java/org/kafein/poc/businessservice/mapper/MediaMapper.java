package org.kafein.poc.businessservice.mapper;

import org.kafein.poc.businessservice.entity.Media;
import org.kafein.poc.businessservice.entity.MediaDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface MediaMapper extends AbstractMapper<Media, MediaDto> {
}
