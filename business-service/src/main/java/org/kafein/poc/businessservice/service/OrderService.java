package org.kafein.poc.businessservice.service;

import lombok.RequiredArgsConstructor;
import org.kafein.poc.businessservice.entity.Order;
import org.kafein.poc.businessservice.mapper.OrderMapper;
import org.kafein.poc.businessservice.repository.BaseRepository;
import org.kafein.poc.businessservice.repository.OrderRepository;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class OrderService extends AbstractService<Order> {
	
	private final OrderRepository orderRepository;
	
	private final OrderMapper orderMapper;
	
	@Override
	public BaseRepository<Order> getRepository() {
		return orderRepository;
	}
}
