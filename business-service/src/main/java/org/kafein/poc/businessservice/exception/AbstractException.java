package org.kafein.poc.businessservice.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.kafein.poc.businessservice.constant.MessageType;
import org.springframework.http.HttpStatus;

@AllArgsConstructor
@NoArgsConstructor
@Data
public abstract class AbstractException extends RuntimeException {
	
	private MessageType messageType = MessageType.API_ERROR;
	private HttpStatus httpStatus;
	private String returnCode;
	private String message;
	private Object[] messageParams;
	
	public AbstractException(String message) {
		this.message = message;
	}
	
	public AbstractException(String returnCode, HttpStatus httpStatus) {
		this.returnCode = returnCode;
		this.httpStatus = httpStatus;
	}
	
	public AbstractException(HttpStatus httpStatus, String returnCode, Object[] messageParams) {
		this.httpStatus = httpStatus;
		this.returnCode = returnCode;
		this.messageParams = messageParams;
	}
	
	public AbstractException(HttpStatus httpStatus, String returnCode, String message) {
		this.httpStatus = httpStatus;
		this.returnCode = returnCode;
		this.message = message;
	}
	
	public AbstractException(HttpStatus httpStatus, String returnCode, String message, Object[] messageParams) {
		this.httpStatus = httpStatus;
		this.returnCode = returnCode;
		this.message = message;
		this.messageParams = messageParams;
	}
	
	
	public String getMessage() {
		return message;
	}
	
	public HttpStatus getStatus() {
		return httpStatus;
	}
}
