package org.kafein.poc.businessservice.exception;


import org.springframework.http.HttpStatus;

public class ElasticSearchException extends AbstractException {
	
	public ElasticSearchException(String message) {
		super(HttpStatus.BAD_REQUEST, message, message);
	}
}
