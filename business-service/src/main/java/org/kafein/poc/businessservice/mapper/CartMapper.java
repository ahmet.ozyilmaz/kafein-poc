package org.kafein.poc.businessservice.mapper;

import org.kafein.poc.businessservice.dto.CartDto;
import org.kafein.poc.businessservice.entity.Cart;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface CartMapper extends AbstractMapper<Cart, CartDto> {
}
