package org.kafein.poc.businessservice.service;

import lombok.RequiredArgsConstructor;
import org.kafein.poc.businessservice.entity.Product;
import org.kafein.poc.businessservice.mapper.ProductMapper;
import org.kafein.poc.businessservice.repository.BaseRepository;
import org.kafein.poc.businessservice.repository.ProductRepository;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;


@Service
@RequiredArgsConstructor
public class ProductService extends AbstractService<Product> {
	private final ProductRepository productRepository;
	
	private final ProductMapper productMapper;
/*
	@PostConstruct
	public void init() {
		
		for (int i = 0; i < 1000; i++) {
			Product product = new Product();
			product.setName("Product " + i);
			product.setDescription("Product " + i + " Description");
			//random generate price
			product.setPrice(Math.random() * 1000);
			
			productRepository.save(product);
		}
	}

 */
	
	@Cacheable(value = "productCache")
	public List<Product> cacheableFindAll() {
		return productRepository.findAll();
	}
	
	@Override
	public List<Product> getAll() {
		return cacheableFindAll();
	}
	
	@Override
	public BaseRepository<Product> getRepository() {
		return productRepository;
	}
}
