package org.kafein.poc.businessservice.dto;


import lombok.Data;

import java.util.Set;

@Data
public class CartDto extends BaseEntityDto {
	
	private CustomerDto customer;
	
	private Set<CartItemDto> cartItems;
}
