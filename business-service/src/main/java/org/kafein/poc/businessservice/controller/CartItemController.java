package org.kafein.poc.businessservice.controller;

import lombok.RequiredArgsConstructor;
import org.kafein.poc.businessservice.dto.CartItemDto;
import org.kafein.poc.businessservice.entity.CartItem;
import org.kafein.poc.businessservice.mapper.AbstractMapper;
import org.kafein.poc.businessservice.mapper.CartItemMapper;
import org.kafein.poc.businessservice.service.AbstractService;
import org.kafein.poc.businessservice.service.CartItemService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/cartItem")
@RequiredArgsConstructor
public class CartItemController extends AbstractController<CartItem, CartItemDto> {
	
	private final CartItemService cartItemService;
	
	private final CartItemMapper cartItemMapper;
	
	@Override
	public AbstractService<CartItem> getService() {
		return cartItemService;
	}
	
	@Override
	public AbstractMapper<CartItem, CartItemDto> getMapper() {
		return cartItemMapper;
	}
}
