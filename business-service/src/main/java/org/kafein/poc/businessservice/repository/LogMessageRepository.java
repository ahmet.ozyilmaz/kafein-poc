package org.kafein.poc.businessservice.repository;

import org.kafein.poc.businessservice.entity.LogMessage;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LogMessageRepository extends JpaRepository<LogMessage, Long> {}

