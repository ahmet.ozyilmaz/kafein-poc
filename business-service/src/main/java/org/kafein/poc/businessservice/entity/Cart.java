package org.kafein.poc.businessservice.entity;


import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "cart")
public class Cart extends BaseEntity {
	
	@OneToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;
	
	@OneToMany(mappedBy = "cart")
	private Set<CartItem> cartItems;
}
