package org.kafein.poc.businessservice.repository;

import org.kafein.poc.businessservice.entity.UploadFile;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface UploadFileRepository extends CrudRepository<UploadFile, Long> {
	
	UploadFile findUploadFileById(Long id);
}
