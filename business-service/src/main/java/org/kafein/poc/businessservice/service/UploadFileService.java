package org.kafein.poc.businessservice.service;

import lombok.RequiredArgsConstructor;
import org.kafein.poc.businessservice.entity.UploadFile;
import org.kafein.poc.businessservice.repository.UploadFileRepository;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UploadFileService {
	
	final UploadFileRepository repository;
	
	public UploadFile getUploadFileById(Long id) {
		return repository.findUploadFileById(id);
	}
	
	public void saveUploadFile(UploadFile uploadFile) {
		repository.save(uploadFile);
	}
}