package org.kafein.poc.businessservice.entity;

import javax.persistence.*;

@Entity
@Table(name = "cart_item")
public class CartItem extends BaseEntity {
	
	@ManyToOne
	@JoinColumn(name = "cart_id")
	private Cart cart;
	
	@Column(name = "cart_id", insertable = false, updatable = false)
	private Long cartId;
	
	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;
	
	private Integer quantity;
}