package org.kafein.poc.businessservice.service;

import lombok.RequiredArgsConstructor;
import org.kafein.poc.businessservice.entity.CartItem;
import org.kafein.poc.businessservice.mapper.CartItemMapper;
import org.kafein.poc.businessservice.repository.BaseRepository;
import org.kafein.poc.businessservice.repository.CartItemRepository;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class CartItemService extends AbstractService<CartItem> {
	
	private final CartItemRepository cartItemRepository;
	
	private final CartItemMapper cartItemMapper;
	
	@Override
	public BaseRepository<CartItem> getRepository() {
		return cartItemRepository;
	}
}
