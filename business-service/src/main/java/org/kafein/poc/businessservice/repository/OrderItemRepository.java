package org.kafein.poc.businessservice.repository;

import org.kafein.poc.businessservice.entity.OrderItem;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderItemRepository extends BaseRepository<OrderItem> {
}
