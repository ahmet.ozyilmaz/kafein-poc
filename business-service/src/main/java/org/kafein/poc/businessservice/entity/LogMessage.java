package org.kafein.poc.businessservice.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "log_message")
public class LogMessage {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "message", columnDefinition = "text")
	private String message;
	
	public LogMessage(String message) {
		this.message = message;
	}
}
