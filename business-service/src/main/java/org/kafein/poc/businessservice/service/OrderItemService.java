package org.kafein.poc.businessservice.service;

import lombok.RequiredArgsConstructor;
import org.kafein.poc.businessservice.entity.OrderItem;
import org.kafein.poc.businessservice.mapper.OrderItemMapper;
import org.kafein.poc.businessservice.repository.BaseRepository;
import org.kafein.poc.businessservice.repository.OrderItemRepository;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class OrderItemService extends AbstractService<OrderItem> {
	
	private final OrderItemRepository orderItemRepository;
	
	private final OrderItemMapper orderItemMapper;
	
	@Override
	public BaseRepository<OrderItem> getRepository() {
		return orderItemRepository;
	}
}
