package org.kafein.poc.businessservice.repository;

import org.kafein.poc.businessservice.entity.CartItem;
import org.springframework.stereotype.Repository;

@Repository
public interface CartItemRepository extends BaseRepository<CartItem> {
}
