package org.kafein.poc.businessservice.controller;

import lombok.RequiredArgsConstructor;
import org.kafein.poc.businessservice.dto.OrderDto;
import org.kafein.poc.businessservice.entity.Order;
import org.kafein.poc.businessservice.mapper.AbstractMapper;
import org.kafein.poc.businessservice.mapper.OrderMapper;
import org.kafein.poc.businessservice.service.AbstractService;
import org.kafein.poc.businessservice.service.OrderService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/order")
@RequiredArgsConstructor
public class OrderController extends AbstractController<Order, OrderDto> {
	
	private final OrderService orderService;
	
	private final OrderMapper orderMapper;
	
	@Override
	public AbstractService<Order> getService() {
		return orderService;
	}
	
	@Override
	public AbstractMapper<Order, OrderDto> getMapper() {
		return orderMapper;
	}
}
