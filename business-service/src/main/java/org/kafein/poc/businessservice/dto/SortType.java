package org.kafein.poc.businessservice.dto;

import org.springframework.data.domain.Sort;

public enum SortType {
	ASC, DESC;
	
	public static Sort.Direction getValue(SortType type) {
		if (type.equals(ASC)) {
			return Sort.Direction.ASC;
		}
		return Sort.Direction.DESC;
	}
}
