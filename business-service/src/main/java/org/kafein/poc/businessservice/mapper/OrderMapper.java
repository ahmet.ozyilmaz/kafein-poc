package org.kafein.poc.businessservice.mapper;

import org.kafein.poc.businessservice.dto.OrderDto;
import org.kafein.poc.businessservice.entity.Order;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface OrderMapper extends AbstractMapper<Order, OrderDto> {
}
