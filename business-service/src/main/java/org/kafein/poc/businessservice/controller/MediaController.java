package org.kafein.poc.businessservice.controller;


import lombok.RequiredArgsConstructor;
import net.coobird.thumbnailator.Thumbnails;
import org.kafein.poc.businessservice.constant.PageableConstants;
import org.kafein.poc.businessservice.dto.SortType;
import org.kafein.poc.businessservice.dto.ThumbnailType;
import org.kafein.poc.businessservice.entity.Media;
import org.kafein.poc.businessservice.entity.MediaDto;
import org.kafein.poc.businessservice.entity.UploadFile;
import org.kafein.poc.businessservice.mapper.AbstractMapper;
import org.kafein.poc.businessservice.mapper.MediaMapper;
import org.kafein.poc.businessservice.service.AbstractService;
import org.kafein.poc.businessservice.service.MediaService;
import org.kafein.poc.businessservice.service.StorageService;
import org.kafein.poc.businessservice.service.UploadFileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/media")
public class MediaController extends AbstractController<Media, MediaDto> {
	
	private final MediaService mediaService;
	private final MediaMapper mediaMapper;
	private final StorageService storageService;
	
	private final UploadFileService uploadFileService;
	
	public AbstractService<Media> getService() {
		return mediaService;
	}
	
	public AbstractMapper<Media, MediaDto> getMapper() {
		return mediaMapper;
	}
	
	Logger logger = LoggerFactory.getLogger(MediaController.class);
	
	@PutMapping("/{id}/upload")
	public ResponseEntity<MediaDto> uploadFile(
			@PathVariable("id") long id, @RequestBody MultipartFile file) throws Exception {
		
		Media media = mediaService.uploadMedia(id, file, null, null);
		return ResponseEntity.ok(getMapper().toDto((media)));
	}
	
	@PostMapping("/upload")
	public ResponseEntity<MediaDto> uploadFile(@RequestBody MultipartFile file,
	                                           @RequestParam("productId") Long productId,
	                                           @RequestParam("imageType") String imageType,
	                                           @RequestParam("fileName") String fileName,
	                                           @RequestParam(required = false) String objectId)
			throws Exception {
		Media media = mediaService.createMediaAndUploadFile(null, file, productId, imageType, fileName, objectId);
		return ResponseEntity.ok(getMapper().toDto((media)));
	}
	
	@GetMapping("/expertiz-file/{productId}/all")
	public ResponseEntity<List<MediaDto>> allByExpertizFile(@PathVariable Long productId) {
		return ResponseEntity.ok(
				getMapper().toDto(mediaService.getAllMediaByProductId(productId)));
	}
	
	@GetMapping("/{id}/download")
	public ResponseEntity<byte[]> downloadFile(@PathVariable Long id) throws IOException {
		return mediaService.downloadFile(id);
	}
	
	@GetMapping("/objectId/{objectId}/download")
	public ResponseEntity<byte[]> downloadFile(@PathVariable String objectId) throws IOException {
		Media media = mediaService.getMediaByObjectId(objectId);
		return mediaService.downloadFile(media.getId());
	}
	
	@GetMapping(value = "/expertiz-file/{productId}/download", produces = "application/zip")
	public ResponseEntity<byte[]> downloadMultipleFile(@PathVariable Long productId) throws IOException {
		return mediaService.downloadMultipleFile(productId);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity delete(@PathVariable Long id) {
		Media media = mediaService.get(id);
		storageService.delete(media.getObjectId());
		this.mediaService.delete(id);
		return ResponseEntity.ok().build();
	}
	
	@DeleteMapping("/delete/{fileId}/all")
	public ResponseEntity deleteAllByProductId(@PathVariable Long fileId) {
		mediaService.deleteAllMediaByProductId(fileId);
		return ResponseEntity.ok().build();
	}
	
	@GetMapping("/make/{id}/{thumbnailType}")
	public void make(@PathVariable Long id, @PathVariable ThumbnailType thumbnailType) {
		
		UploadFile uploadFile = uploadFileService.getUploadFileById(id);
		String oldPathname = uploadFile.getOldPath();
		try {
			String thumbnailPathname = uploadFile.getOldPath().replace(".", thumbnailType.width + "_" + thumbnailType.height + ".");
			uploadFile.setNewPath(thumbnailPathname);
			uploadFileService.saveUploadFile(uploadFile);
			Thumbnails.of(oldPathname).size(thumbnailType.width, thumbnailType.height).toFile(thumbnailPathname);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	@GetMapping("/{fileId}/all")
	public ResponseEntity<Page<MediaDto>> getPaginatedMediaByIdAndImageType(@PathVariable Long fileId,
	                                                                        @RequestParam(defaultValue = PageableConstants.PAGE_NO_DEFAULT_VALUE) Integer pageNo,
	                                                                        @RequestParam(defaultValue = PageableConstants.PAGE_SIZE_DEFAULT_VALUE) Integer pageSize,
	                                                                        @RequestParam(defaultValue = PageableConstants.PROPERTY_DEFAULT_VALUE) String property,
	                                                                        @RequestParam(defaultValue = PageableConstants.PAGE_SORT_DEFAULT_VALUE) SortType sort) {
		Pageable pageable = PageRequest.of(pageNo, pageSize, Sort.by(SortType.getValue(sort), property));
		return ResponseEntity.ok(mediaService.getPaginatedMediaByProductId(fileId, pageable));
	}
	
}
