package org.kafein.poc.businessservice.controller;

import lombok.RequiredArgsConstructor;
import org.kafein.poc.businessservice.dto.ProductDto;
import org.kafein.poc.businessservice.entity.Product;
import org.kafein.poc.businessservice.mapper.AbstractMapper;
import org.kafein.poc.businessservice.mapper.ProductMapper;
import org.kafein.poc.businessservice.service.AbstractService;
import org.kafein.poc.businessservice.service.ProductService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/product")
@RequiredArgsConstructor
public class ProductController extends AbstractController<Product, ProductDto> {
	
	private final ProductService productService;
	
	private final ProductMapper productMapper;
	
	@Override
	public AbstractService<Product> getService() {
		return productService;
	}
	
	@Override
	public AbstractMapper<Product, ProductDto> getMapper() {
		return productMapper;
	}
}
