package org.kafein.poc.businessservice.service;

import lombok.RequiredArgsConstructor;
import org.kafein.poc.businessservice.entity.Cart;
import org.kafein.poc.businessservice.mapper.CartMapper;
import org.kafein.poc.businessservice.repository.BaseRepository;
import org.kafein.poc.businessservice.repository.CartRepository;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class CartService extends AbstractService<Cart> {
	
	private final CartRepository cartRepository;
	
	private final CartMapper cartMapper;
	
	@Override
	public BaseRepository<Cart> getRepository() {
		return cartRepository;
	}
}
