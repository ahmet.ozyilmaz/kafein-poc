package org.kafein.poc.businessservice.dto;

import lombok.Data;

@Data
public class OrderItemDto extends BaseEntityDto {
	
	private OrderDto order;
	
	private ProductDto product;
	
	private Integer quantity;
	
	private Double unitPrice;
}