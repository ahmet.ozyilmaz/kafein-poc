package org.kafein.poc.businessservice.service;

import com.amazonaws.services.s3.model.S3Object;
import com.amazonaws.services.s3.model.S3ObjectInputStream;
import lombok.RequiredArgsConstructor;
import org.apache.commons.collections4.ListUtils;
import org.apache.commons.io.IOUtils;
import org.kafein.poc.businessservice.entity.Media;
import org.kafein.poc.businessservice.entity.MediaDto;
import org.kafein.poc.businessservice.exception.MediaExistsException;
import org.kafein.poc.businessservice.exception.NotFoundException;
import org.kafein.poc.businessservice.mapper.MediaMapper;
import org.kafein.poc.businessservice.repository.BaseRepository;
import org.kafein.poc.businessservice.repository.MediaRepository;
import org.kafein.poc.businessservice.util.Base64Util;
import org.kafein.poc.businessservice.util.ZipUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.zip.ZipOutputStream;

@RequiredArgsConstructor
@Service
public class MediaService extends AbstractService<Media> {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MediaService.class);
	
	private final MediaRepository mediaRepository;
	
	private final StorageService storageService;
	
	private final ZipUtil zipUtil;
	
	private final MediaMapper mediaMapper;
	
	
	@Override
	public BaseRepository<Media> getRepository() {
		return mediaRepository;
	}
	
	public Media getMediaByObjectId(String objectId) {
		return mediaRepository.findByObjectId(objectId);
	}
	
	public ResponseEntity<byte[]> downloadFile(Long id) throws IOException {
		
		Media media = get(id);
		if (media == null) {
			throw new NotFoundException("Image not found!");
		}
		
		byte[] bytes;
		try (S3Object s3Object = storageService.download(media.getObjectId())) {
			S3ObjectInputStream inputStream = s3Object.getObjectContent();
			bytes = IOUtils.toByteArray(inputStream);
			inputStream.close();
		}
		
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
		httpHeaders.setContentLength(bytes.length);
		httpHeaders.setContentDispositionFormData("attachment", media.getFileName());
		
		return new ResponseEntity<>(bytes, httpHeaders, HttpStatus.OK);
	}
	
	public Media createMediaAndUploadFile(Long companyId, MultipartFile file, Long productId, String imageType, String fileName, String objectId)
			throws Exception {
		Media media = new Media();
		media.setFileName(fileName);
		media.setIsActive(true);
		if (Objects.nonNull(objectId) && mediaRepository.existsByObjectId(objectId)) {
			throw new MediaExistsException("Media already exists");
		}
		var savedMedia = super.save(media);
		return uploadMedia(savedMedia.getId(), file, companyId, objectId);
	}
	
	public List<Media> getAllMediaByProductId(long id) {
		return mediaRepository.findAllByProductId(id);
	}
	
	public ResponseEntity<byte[]> downloadMultipleFile(Long productId) throws IOException {
		var mediasToZip = getAllMediaByProductId(productId);
		
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ZipOutputStream zipOutputStream = new ZipOutputStream(baos);
		ListUtils.emptyIfNull(mediasToZip).forEach(media -> zipUtil.addMediaFileToZipStream(media, zipOutputStream));
		zipOutputStream.close();
		byte[] bytes = baos.toByteArray();
		try {
			HttpHeaders httpHeaders = new HttpHeaders();
			httpHeaders.setContentLength(bytes.length);
			httpHeaders.setContentType(MediaType.valueOf("application/zip"));
			httpHeaders.add("Content-Disposition", "attachment; filename=\"resimler.zip\"");
			return new ResponseEntity<>(bytes, httpHeaders, HttpStatus.OK);
		} catch (Exception e) {
			return null;
		}
		
	}
	
	@Transactional
	public Media uploadMedia(long id, MultipartFile file, Long companyId, String objectId)
			throws Exception {
		
		if (objectId != null) {
			return uploadMediaMobil(id, file, companyId, objectId);
		}
		
		InputStream fileInputStream = file.getInputStream();
		byte[] content = fileInputStream.readAllBytes();
		fileInputStream.close();
		String generatedObjectId = storageService.createUUID();
		storageService.put(generatedObjectId, new ByteArrayInputStream(content), (int) file.getSize());
		Media media = get(id);
		
		String[] parseFileName = Objects.requireNonNull(file.getOriginalFilename()).split("\\.");
		String mediaFormat = parseFileName[(parseFileName.length) - 1].toLowerCase(Locale.ROOT);
		try {
			if (mediaFormat.equals("jpg") || mediaFormat.equals("png") || mediaFormat.equals("jpeg")) {
				String thumbnail =
						Base64Util.encoder(Base64Util.resize(content), mediaFormat);
				media.setThumbnail(thumbnail);
			}
		} catch (Exception e) {
			LOGGER.warn("Thumbnail could not create for media(id={})", media.getId());
		}
		
		media.setSize(getByteToMegaByte(file.getSize()));
		media.setFileName(file.getOriginalFilename());
		media.setObjectId(generatedObjectId);
		
		media = put(id, media);
		
		return media;
	}
	
	public Double getByteToMegaByte(double size) {
		return size / 1024 / 1024;
	}
	
	@Transactional
	public Media uploadMediaMobil(long id, MultipartFile file, Long companyId, String objectId) throws IOException {
		
		InputStream fileInputStream = file.getInputStream();
		byte[] content = fileInputStream.readAllBytes();
		fileInputStream.close();
		
		storageService.put(objectId, new ByteArrayInputStream(content), (int) file.getSize());
		
		Media media = get(id);
		
		String[] parseFileName = Objects.requireNonNull(file.getOriginalFilename()).split("\\.");
		String mediaFormat = parseFileName[(parseFileName.length) - 1].toLowerCase(Locale.ROOT);
		try {
			if (mediaFormat.equals("jpg") || mediaFormat.equals("png") || mediaFormat.equals("jpeg")) {
				String thumbnail =
						Base64Util.encoder(Base64Util.resize(content), mediaFormat);
				media.setThumbnail(thumbnail);
			}
		} catch (Exception e) {
			LOGGER.warn("Thumbnail could not create for media(id={})", media.getId());
		}
		
		media.setSize(getByteToMegaByte(file.getSize()));
		media.setFileName(file.getOriginalFilename());
		media.setObjectId(objectId);
		
		media = put(id, media);
		
		return media;
	}
	
	@Transactional
	public void deleteAllMediaByProductId(long id) {
		mediaRepository.deleteAllByProductId(id);
	}
	
	
	@Transactional
	public void delete(Long id) {
		Media media = get(id);
		this.mediaRepository.deleteByIdNativeInvoiceMediaMapping(id);
		this.mediaRepository.deleteByIdNative(id);
	}
	
	@Transactional
	public void deleteOnSupportLink(Long id, Long companyId, String receiver, Long fileId) {
		this.mediaRepository.deleteByIdNativeInvoiceMediaMapping(id);
		this.mediaRepository.deleteByIdNative(id);
	}
	
	public Page<MediaDto> getPaginatedMediaByProductId(long id, Pageable pageable) {
		Page<Media> mediaPage = mediaRepository.findAllByProductId(id, pageable);
		return new PageImpl<>(mediaMapper.toDto(mediaPage.getContent()), pageable, mediaPage.getTotalElements());
	}
	
}
