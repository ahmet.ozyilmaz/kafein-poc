package org.kafein.poc.businessservice.util;

import com.amazonaws.services.s3.model.S3Object;
import org.apache.commons.io.IOUtils;
import org.kafein.poc.businessservice.entity.Media;
import org.kafein.poc.businessservice.exception.InternalServerError;
import org.kafein.poc.businessservice.service.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Service
public class ZipUtil {
	
	@Autowired
	private StorageService storageService;
	
	public void addMediaFileToZipStream(Media mediaEntity, ZipOutputStream zipOutputStream) {
		try {
			String entryName = createZipEntryName(mediaEntity);
			zipOutputStream.putNextEntry(new ZipEntry(entryName));
			
			byte[] bytes;
			try (S3Object s3Object = storageService.download(mediaEntity.getObjectId())) {
				InputStream inputStream = s3Object.getObjectContent();
				bytes = IOUtils.toByteArray(inputStream);
			}
			zipOutputStream.write(bytes);
			
			zipOutputStream.closeEntry();
		} catch (IOException e) {
			throw new InternalServerError(e.getMessage());
		}
	}
	
	private String createZipEntryName(Media media) {
		String[] split = media.getFileName().split("\\.");
		String imageFormat = split[split.length - 1];
		
		return media.getFileName()
				+ "-"
				+ media.getObjectId().substring(0, 5)
				+ "."
				+ imageFormat;
	}
}
