package org.kafein.poc.businessservice.entity;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Set;


@Entity
@Table(name = "customer")
public class Customer extends BaseEntity {
	
	@OneToMany(mappedBy = "customer")
	private Set<Order> orders;
	
	@OneToOne(mappedBy = "customer")
	private Cart cart;
}