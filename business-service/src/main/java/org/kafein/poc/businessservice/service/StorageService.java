package org.kafein.poc.businessservice.service;

import com.amazonaws.ClientConfiguration;
import com.amazonaws.Protocol;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.*;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;
import org.kafein.poc.businessservice.exception.InternalServerError;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.InputStream;
import java.util.Base64;
import java.util.UUID;

@Service
public class StorageService {
	
	private AmazonS3 s3Client;
	
	@Value("${s3.bucket-name}")
	private String s3BucketName;
	
	@Value("${s3.url}")
	private String s3Url;
	@Value("${s3.username}")
	private String s3Username;
	@Value("${s3.password}")
	private String s3Password;
	
	
	@PostConstruct
	public void init() {
		ClientConfiguration clientConfiguration = new ClientConfiguration();
		clientConfiguration.setMaxErrorRetry(3);
		clientConfiguration.setConnectionTimeout(100 * 1000);
		clientConfiguration.setSocketTimeout(100 * 1000);
		clientConfiguration.setProtocol(Protocol.HTTP);
		
		s3Client = AmazonS3ClientBuilder.standard()
				.withClientConfiguration(clientConfiguration)
				.withEndpointConfiguration(
						new AwsClientBuilder.EndpointConfiguration(
								s3Url, Regions.US_EAST_1.name()))
				.withPathStyleAccessEnabled(true)
				.withCredentials(
						new AWSCredentialsProvider() {
							@Override
							public AWSCredentials getCredentials() {
								return new BasicAWSCredentials(s3Username, s3Password);
							}
							
							@Override
							public void refresh() {
							}
						}).build();
	}
	
	public String put(String objectId, InputStream inputStream, int length) {
		try {
			ObjectMetadata metadata = new ObjectMetadata();
			metadata.setContentLength(length);
			PutObjectRequest request =
					new PutObjectRequest(s3BucketName, objectId, inputStream, metadata);
			request.setMetadata(metadata);
			PutObjectResult result = s3Client.putObject(request);
			
			byte[] decoded = Base64.getDecoder().decode(result.getContentMd5());
			return String.valueOf(Hex.encodeHex(decoded));
		} catch (SdkClientException e) {
			throw new InternalServerError(e.getMessage());
		}
	}
	
	public S3Object download(String objectId) {
		GetObjectRequest request = new GetObjectRequest(s3BucketName, objectId);
		try {
			return s3Client.getObject(request);
		} catch (Exception e) {
			throw new InternalServerError(e.getMessage());
		}
	}
	
	public InputStream downloadAsync(String objectId) {
		GetObjectRequest request = new GetObjectRequest(s3BucketName, objectId);
		try {
			return s3Client.getObject(request).getObjectContent();
		} catch (Exception e) {
			throw new InternalServerError(e.getMessage());
		}
	}
	
	public void delete(String objectId) {
		try {
			if (StringUtils.isEmpty(objectId)) {
				return;
			}
			s3Client.deleteObject(new DeleteObjectRequest(s3BucketName, objectId));
		} catch (SdkClientException e) {
			throw new InternalServerError(e.getMessage());
		}
	}
	
	public void copy(String sourceObjectId, String targetObjectId) {
		try {
			CopyObjectRequest request = new CopyObjectRequest();
			request.setSourceBucketName(s3BucketName);
			request.setDestinationBucketName(s3BucketName);
			request.setSourceKey(sourceObjectId);
			request.setDestinationKey(targetObjectId);
			s3Client.copyObject(request);
		} catch (SdkClientException e) {
			throw new InternalServerError(e.getMessage());
		}
	}
	
	public String createUUID() {
		UUID uuid = UUID.randomUUID();
		return uuid.toString();
	}
}
