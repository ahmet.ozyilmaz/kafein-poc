package org.kafein.poc.businessservice.dto;

import lombok.Data;

import java.util.Date;
import java.util.Set;


@Data
public class OrderDto extends BaseEntityDto {
	
	private CustomerDto customer;
	
	private Date orderDate;
	
	private String status;
	
	private Set<OrderItemDto> orderItems;
}