package org.kafein.poc.businessservice.exception;

import org.springframework.http.HttpStatus;

public class SubscriptionExpiredException extends AbstractException {
	
	public SubscriptionExpiredException(Object... messageParams) {
		super(HttpStatus.BAD_REQUEST, "SUBSCRIPTION_EXPIRED", messageParams);
	}
	
}
