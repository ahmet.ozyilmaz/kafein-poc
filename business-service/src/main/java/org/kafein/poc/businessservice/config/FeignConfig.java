package org.kafein.poc.businessservice.config;

import feign.RequestInterceptor;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.http.HttpServletRequest;

@Configuration
@RequiredArgsConstructor
public class FeignConfig {
	private final HttpServletRequest request;
	
	@Bean
	public RequestInterceptor requestInterceptor() {
		String headerKey = "Authorization";
		return requestTemplate -> {
			requestTemplate.header(headerKey, request.getHeader(headerKey));
		};
	}
}
