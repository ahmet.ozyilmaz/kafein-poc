package org.kafein.poc.businessservice.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class BaseEntityDto implements Serializable {
	
	private Long id;
	
	private Date creationDate;
	
	private Date lastUpdateDate;
	
	private Boolean isActive;
	
	private Boolean isDeleted;
	
}
