package org.kafein.poc.businessservice.mapper;

import org.kafein.poc.businessservice.dto.ProductDto;
import org.kafein.poc.businessservice.entity.Product;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProductMapper extends AbstractMapper<Product, ProductDto> {
}
