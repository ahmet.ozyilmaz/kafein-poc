package org.kafein.poc.businessservice.repository;

import org.kafein.poc.businessservice.entity.Customer;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepository extends BaseRepository<Customer> {
}
