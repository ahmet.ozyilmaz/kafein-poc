package org.kafein.poc.businessservice.repository;

import org.kafein.poc.businessservice.entity.Product;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends BaseRepository<Product> {
}
