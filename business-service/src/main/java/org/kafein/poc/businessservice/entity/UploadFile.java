package org.kafein.poc.businessservice.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity(name = "upload_file")
public class UploadFile {
	
	@Id
	private Long id;
	
	@Column
	private String fileName;
	
	@Column
	private String oldPath;
	
	@Column
	private String newPath;
}