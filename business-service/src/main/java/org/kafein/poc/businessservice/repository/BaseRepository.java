package org.kafein.poc.businessservice.repository;

import org.kafein.poc.businessservice.entity.BaseEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface BaseRepository<T extends BaseEntity>
		extends JpaRepository<T, Long>, JpaSpecificationExecutor<T> {
	
	Page<T> findAll(Specification<T> var1, Pageable pageable);
	
}
