package org.kafein.poc.businessservice.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "media")
public class Media extends BaseEntity {
	
	@ManyToOne
	@JoinColumn(name = "product_id")
	private Product product;

	@Column(name = "product_id", insertable = false, updatable = false)
	private Long productId;
	
	@Column(name = "object_id")
	private String objectId;
	
	@Lob
	@Column(name = "thumbnail")
	private String thumbnail;
	
	@Column(name = "description")
	private String description;
	
	
	@Column(name = "file_name")
	private String fileName;
	
	@Column(name = "size")
	private Double size;
}
