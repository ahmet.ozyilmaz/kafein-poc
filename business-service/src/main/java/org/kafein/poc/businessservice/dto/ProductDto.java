package org.kafein.poc.businessservice.dto;

import lombok.Data;
import org.kafein.poc.businessservice.entity.MediaDto;

import java.util.Set;

@Data
public class ProductDto extends BaseEntityDto {
	
	private String name;
	private String description;
	private Double price;
	private Integer stock;
	
	private Set<MediaDto> mediaSet;
}