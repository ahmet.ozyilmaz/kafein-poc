package org.kafein.poc.businessservice.repository;

import org.kafein.poc.businessservice.entity.Media;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MediaRepository extends BaseRepository<Media> {
	List<Media> findAllByProductId(long productId);
	
	Media findByObjectId(String objectId);
	
	void deleteAllByProductId(long productId);
	
	
	@Query(value = "SELECT COALESCE(SUM(m.size), 0) FROM Media m WHERE m.productId in (?1)")
	Double sumSizeByProductIds(List<Long> productIds);
	
	@Modifying
	@Query(value = "DELETE FROM invoice_media_list WHERE media_list_id = ?1", nativeQuery = true)
	void deleteByIdNativeInvoiceMediaMapping(long id);
	
	@Modifying
	@Query(value = " DELETE from media WHERE id = ?1 ", nativeQuery = true)
	void deleteByIdNative(long id);
	
	Page<Media> findAllByProductId(long productId, Pageable pageable);
	
	boolean existsByObjectId(String objectId);
}
