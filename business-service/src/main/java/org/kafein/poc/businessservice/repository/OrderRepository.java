package org.kafein.poc.businessservice.repository;

import org.kafein.poc.businessservice.entity.Order;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends BaseRepository<Order> {
}
