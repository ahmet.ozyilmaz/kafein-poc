package org.kafein.poc.businessservice.entity;

import javax.persistence.*;
import java.util.Date;
import java.util.Set;


@Entity
@Table(name = "order")
public class Order extends BaseEntity {
	
	@ManyToOne
	@JoinColumn(name = "customer_id")
	private Customer customer;
	
	private Date orderDate;
	
	private String status;
	
	@OneToMany(mappedBy = "order")
	private Set<OrderItem> orderItems;
}