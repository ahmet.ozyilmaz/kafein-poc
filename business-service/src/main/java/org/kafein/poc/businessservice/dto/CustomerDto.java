package org.kafein.poc.businessservice.dto;

import lombok.Data;

import java.util.Set;


@Data
public class CustomerDto extends BaseEntityDto {
	
	private Set<OrderDto> orders;
	
	private CartDto cart;
}