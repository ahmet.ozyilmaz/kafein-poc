package org.kafein.poc.businessservice.util;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.MetadataException;
import com.drew.metadata.exif.ExifIFD0Directory;
import org.kafein.poc.businessservice.dto.ImageOrientationType;
import org.kafein.poc.businessservice.exception.InternalServerError;
import org.kafein.poc.businessservice.exception.NotFoundException;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Base64;

public class Base64Util {
	
	static int imageScaledSize = 300;
	
	public static BufferedImage resize(byte[] bytes) {
		
		BufferedImage inputImage;
		try {
			inputImage = ImageIO.read(new ByteArrayInputStream(bytes));
		} catch (Exception e) {
			e.printStackTrace();
			throw new InternalServerError("Resim okunurken hata meydana geldi");
		}
		
		int scaledWidth;
		int scaledHeight;
		double percent;
		
		if (inputImage.getWidth() > inputImage.getHeight()) {
			
			scaledWidth = imageScaledSize;
			percent = imageScaledSize / (double) inputImage.getWidth();
			
			scaledHeight = (int) (inputImage.getHeight() * percent);
		} else {
			scaledHeight = imageScaledSize;
			percent = imageScaledSize / (double) inputImage.getHeight();
			
			scaledWidth = (int) (inputImage.getWidth() * percent);
		}
		
		BufferedImage outputImage = new BufferedImage(scaledWidth, scaledHeight, inputImage.getType());
		
		Graphics2D g2d = outputImage.createGraphics();
		Directory exif = null;
		try {
			exif = ImageMetadataReader.readMetadata(new ByteArrayInputStream(bytes)).getFirstDirectoryOfType(ExifIFD0Directory.class);
		} catch (ImageProcessingException | IOException exifException) {
			exifException.printStackTrace();
		}
		if (exif != null && exif.containsTag(ExifIFD0Directory.TAG_ORIENTATION)) {
			AffineTransform transform = orientationTransform(outputImage, exif);
			g2d.setTransform(transform);
		}
		g2d.drawImage(inputImage, 0, 0, scaledWidth, scaledHeight, null);
		g2d.dispose();
		
		return outputImage;
	}
	
	public static String encoder(BufferedImage outputImage, String format) {
		
		String base64Image;
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			ImageIO.write(outputImage, format, out);
			
			byte[] bytes = out.toByteArray();
			base64Image = Base64.getEncoder().encodeToString(bytes);
			
		} catch (FileNotFoundException e) {
			throw new NotFoundException("Resim Bulunamadı");
		} catch (IOException ioe) {
			throw new InternalServerError("Resim okunurken hata meydana geldi");
		}
		
		return base64Image;
	}
	
	public static AffineTransform orientationTransform(BufferedImage outputImage, Directory exif) {
		AffineTransform transform = new AffineTransform();
		int orientation = 1;
		try {
			orientation = exif.getInt(ExifIFD0Directory.TAG_ORIENTATION);
		} catch (MetadataException metadataException) {
			metadataException.printStackTrace();
		}
		switch (ImageOrientationType.valueOf(orientation)) {
			case HORIZONTAL_NORMAL:
				break;
			case MIRROR_HORIZONTAL:
				transform.scale(-1.0, 1.0);
				transform.translate(-outputImage.getWidth(), 0);
				break;
			case ROTATE_180:
				transform.translate(outputImage.getWidth(), outputImage.getHeight());
				transform.rotate(Math.PI);
				break;
			case MIRROR_VERTICAL:
				transform.scale(1.0, -1.0);
				transform.translate(0, -outputImage.getHeight());
				break;
			case MIRROR_HORIZONTAL_ROTATE_270_CW:
				transform.rotate(3 * Math.PI / 2);
				transform.scale(-1.0, 1.0);
				transform.translate(-outputImage.getWidth(), 0);
				break;
			case ROTATE_90_CW:
				transform.rotate(Math.PI / 2);
				transform.translate(0, -outputImage.getWidth());
				break;
			case MIRROR_HORIZONTAL_ROTATE_90_CW:
				transform.rotate(Math.PI / 2);
				transform.scale(-1.0, 1.0);
				transform.translate(-outputImage.getHeight(), 0);
				break;
			case ROTATE_270_CW:
				transform.rotate(3 * Math.PI / 2);
				transform.translate(-outputImage.getHeight(), 0);
				break;
		}
		return transform;
	}
}
