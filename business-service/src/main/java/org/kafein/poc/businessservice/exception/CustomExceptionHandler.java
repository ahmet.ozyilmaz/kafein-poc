package org.kafein.poc.businessservice.exception;

//import com.mysql.cj.jdbc.exceptions.MysqlDataTruncation;

import lombok.extern.log4j.Log4j2;
import org.kafein.poc.businessservice.dto.ErrorDto;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Log4j2
@ControllerAdvice
public class CustomExceptionHandler extends ResponseEntityExceptionHandler {
	
	@Override
	public ResponseEntity<Object> handleHttpMessageNotReadable(
			HttpMessageNotReadableException ex,
			HttpHeaders headers,
			HttpStatus status,
			WebRequest request) {
		ErrorDto dto = new ErrorDto();
		dto.setResultCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
		dto.setErrorMessage(ex.getMessage());
		dto.setResult(HttpStatus.INTERNAL_SERVER_ERROR.name());
		return handleExceptionInternal(
				ex, dto, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
	}
	
	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(
			MethodArgumentNotValidException ex,
			HttpHeaders headers,
			HttpStatus status,
			WebRequest request) {
		ErrorDto dto = new ErrorDto();
		dto.setResultCode(HttpStatus.BAD_REQUEST.value());
		dto.setErrorMessage(ex.getFieldErrors().get(0).getDefaultMessage());
		dto.setResult(HttpStatus.BAD_REQUEST.name());
		
		return handleExceptionInternal(ex, dto, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
	}
	
	@ExceptionHandler
	protected ResponseEntity<Object> handleInternalServerError(
			InternalServerError ex, WebRequest request) {
		ErrorDto dto = new ErrorDto();
		dto.setResultCode(ex.getStatus().value());
		dto.setErrorMessage(ex.getMessage());
		dto.setResult(ex.getStatus().name());
		return handleExceptionInternal(ex, dto, new HttpHeaders(), ex.getStatus(), request);
	}
	
	@ExceptionHandler
	protected ResponseEntity<Object> handleNotFoundException(
			NotFoundException ex, WebRequest request) {
		ErrorDto dto = new ErrorDto();
		dto.setResultCode(ex.getStatus().value());
		dto.setErrorMessage(ex.getMessage());
		dto.setResult(ex.getStatus().name());
		return handleExceptionInternal(ex, dto, new HttpHeaders(), ex.getStatus(), request);
	}
	
	@ExceptionHandler
	protected ResponseEntity<Object> handleForceUpdateException(
			ForceUpdateException ex, WebRequest request) {
		ErrorDto dto = new ErrorDto();
		dto.setResultCode(ex.getStatus().value());
		dto.setErrorMessage(ex.getMessage());
		dto.setResult(ex.getStatus().name());
		return handleExceptionInternal(ex, dto, new HttpHeaders(), ex.getStatus(), request);
	}
	
	@ExceptionHandler
	protected ResponseEntity<Object> handleUnSupportedOperationException(
			UnSupportedOperationException ex, WebRequest request) {
		ErrorDto dto = new ErrorDto();
		dto.setResultCode(ex.getStatus().value());
		dto.setErrorMessage(ex.getMessage());
		dto.setResult(ex.getStatus().name());
		return handleExceptionInternal(ex, dto, new HttpHeaders(), ex.getStatus(), request);
	}
	
	@ExceptionHandler
	protected ResponseEntity<Object> handleBadRequestException(
			BadRequestException ex, WebRequest request) {
		ErrorDto dto = new ErrorDto();
		dto.setResultCode(ex.getStatus().value());
		dto.setErrorMessage(ex.getMessage());
		dto.setResult(ex.getStatus().name());
		return handleExceptionInternal(ex, dto, new HttpHeaders(), ex.getStatus(), request);
	}
	
	@ExceptionHandler
	protected ResponseEntity<Object> handleForbiddenException(
			ForbiddenException ex, WebRequest request) {
		ErrorDto dto = new ErrorDto();
		dto.setResultCode(ex.getStatus().value());
		dto.setErrorMessage(ex.getMessage());
		dto.setResult(ex.getStatus().name());
		return handleExceptionInternal(ex, dto, new HttpHeaders(), ex.getStatus(), request);
	}
	
	
	@ExceptionHandler(Throwable.class)
	public ResponseEntity<ErrorDto> handleGeneral(Throwable throwable) {
		log.error("Unhandled Exception when processing request", throwable);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
				.contentType(MediaType.APPLICATION_JSON)
				.body(ErrorDto.builder()
						.resultCode(HttpStatus.INTERNAL_SERVER_ERROR.value())
						.errorMessage("Unhandled Exception when processing request" + throwable.getMessage()).build());
		
	}
	
	/*@ExceptionHandler({QueryException.class, MysqlDataTruncation.class})
	public final ResponseEntity<Object> handleQueryException(
			QueryException ex, WebRequest request, HttpServletRequest httpRequest) {
			ErrorDto dto = new ErrorDto();
			dto.setResultCode(HttpStatus.INTERNAL_SERVER_ERROR.value());
			dto.setErrorMessage(ex.getMessage());
			dto.setResult(HttpStatus.INTERNAL_SERVER_ERROR.name());
			dto.setRequestUrl(httpRequest.getRequestURI());
		return handleExceptionInternal(ex, dto, new HttpHeaders(), HttpStatus.INTERNAL_SERVER_ERROR, request);
	}*/
	@ExceptionHandler
	protected ResponseEntity<Object> handleElasticSearchException(
			ElasticSearchException ex, WebRequest request) {
		ErrorDto dto = new ErrorDto();
		dto.setResultCode(ex.getStatus().value());
		dto.setErrorMessage(ex.getMessage());
		dto.setResult(ex.getStatus().name());
		return handleExceptionInternal(ex, dto, new HttpHeaders(), ex.getStatus(), request);
	}
	
	@ExceptionHandler
	protected ResponseEntity<Object> handleMediaExistsException(
			MediaExistsException ex, WebRequest request) {
		ErrorDto dto = new ErrorDto();
		dto.setResultCode(ex.getStatus().value());
		dto.setErrorMessage(ex.getMessage());
		dto.setResult(ex.getStatus().name());
		return handleExceptionInternal(ex, dto, new HttpHeaders(), ex.getStatus(), request);
	}
	
	@ExceptionHandler
	public ResponseEntity<Object> handleDataIntegrityViolationException(
			DataIntegrityViolationException ex,
			WebRequest request) {
		ErrorDto dto = new ErrorDto();
		dto.setResultCode(HttpStatus.BAD_REQUEST.value());
		dto.setErrorMessage("Bazı değerler kullanılmaktadır.");
		dto.setResult(HttpStatus.BAD_REQUEST.name());
		return handleExceptionInternal(ex, dto, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
	}
}
